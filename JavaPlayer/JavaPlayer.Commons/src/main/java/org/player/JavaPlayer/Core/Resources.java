package org.player.JavaPlayer.Core;

import java.awt.Image;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Main.IResource;

public class Resources implements IResource {

    private ClassLoader cl;
    private Image icon;
    
    public Resources() {
        cl = this.getClass().getClassLoader();

        try {
            icon = ImageIO.read(cl.getResource("icon.png"));
        } catch (IOException e) {
            Logger.getLogger(Constants.APPNAME).log(Level.WARNING, "Icon not loaded");
        }
    }
    
    @Override
    public Image getIcon() {
        return icon;
    }
}
