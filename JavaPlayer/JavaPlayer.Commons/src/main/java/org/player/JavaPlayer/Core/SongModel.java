package org.player.JavaPlayer.Core;

import java.awt.Image;
import java.net.URI;

import org.player.JavaPlayer.Contracts.Common.AbstractProperty;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

public class SongModel extends AbstractProperty implements ISongModel {

    private URI file;
    private String title;
    private String artist;
    private String album;
    private int year;
    private String comment;
    private String genre;
    private String track;
    private Image image;
    private long length;
    private int bitrate;

    @Override
    public URI getURI() {
        return file;
    }
    @Override
    public void setURI(URI file) {
        this.file = file;
    }

    @Override
    public String getTitle() {
        return title;
    }
    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getAlbum() {
        return album;
    }
    @Override
    public void setAlbum(String album) {
        this.album = album;
    }

    @Override
    public int getYear() {
        return year;
    }
    @Override
    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String getComment() {
        return comment;
    }
    @Override
    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String getArtist() {
        return artist;
    }
    @Override
    public void setArtist(String artist) {
        this.artist = artist;
    }

    @Override
    public String getGenre() {
        return genre;
    }
    @Override
    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public String getTrack() {
        return track;
    }
    @Override
    public void setTrack(String track) {
        this.track = track;
    }

    @Override
    public Image getImage() {
        return image;
    }
    @Override
    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public long getLength() {
        return length;
    }
    @Override
    public void setLength(long length) {
        this.length = length;
    }

    @Override
    public int getBitrate() {
        return bitrate;
    }
    @Override
    public void setBitrate(int bitrate) {
        this.bitrate = bitrate;
    }
}
