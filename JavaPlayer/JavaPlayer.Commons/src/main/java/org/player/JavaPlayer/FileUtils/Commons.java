package org.player.JavaPlayer.FileUtils;

import java.io.File;
import java.net.URI;

public class Commons {

    public static String getExtension(File f) {
        String fileName = f.getName();
        int i = fileName.lastIndexOf('.');

        String ext = null;
        if (i > 0 && i < fileName.length() - 1) {
            ext = fileName.substring(i + 1).toLowerCase();
        }
        return ext;
    }

	public static String getExtension(URI uri) {
        String fileName = uri.getPath();
        int i = fileName.lastIndexOf('.');

        String ext = null;
        if (i > 0 && i < fileName.length() - 1) {
            ext = fileName.substring(i + 1).toLowerCase();
        }
        return ext;
	}
}
