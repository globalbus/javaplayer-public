package org.player.JavaPlayer.FileUtils;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.ITagReader;

public class ID3TagReader implements ITagReader {

    @Override
    public void getTags(ISongModel song) {
    	if(!song.getURI().getScheme().equals("file"))
    		return;
        try {
            Mp3File mp3file = new Mp3File(new File(song.getURI()).toString());
            ID3v1 tag = null;
            if (mp3file.hasId3v2Tag()) {
                ID3v2 tagv2 = mp3file.getId3v2Tag();
                tag = tagv2;
                if (tagv2.getAlbumImage() != null) {
                    song.setImage(ImageIO.read(new ByteArrayInputStream(tagv2
                            .getAlbumImage())));
                }

            } else if (mp3file.hasId3v1Tag()) {
                tag = mp3file.getId3v1Tag();
            } else {
                return;
            }
            song.setTitle(tag.getTitle());
            song.setAlbum(tag.getAlbum());
            song.setArtist(tag.getArtist());
            song.setComment(tag.getComment());
            song.setGenre(tag.getGenreDescription());
            if (tag.getTrack() != null) {
                song.setTrack(tag.getTrack());
            }
            if (tag.getYear() != null && tag.getYear().length()>0) {
                song.setYear(Integer.parseInt(tag.getYear()));
            }
            song.setLength(mp3file.getLengthInSeconds());
            song.setBitrate(mp3file.getBitrate());
        } catch (UnsupportedTagException | InvalidDataException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
