package org.player.JavaPlayer.FileUtils;

import java.io.File;
import javax.swing.filechooser.FileFilter;

import org.player.JavaPlayer.Constants.SupportedFileTypes;

public class MusicFileFilter extends FileFilter {

	
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		String extension = Commons.getExtension(f);
		if (extension != null) {
			if (extension.equals(SupportedFileTypes.MP3))
				return true;
			else if (extension.equals(SupportedFileTypes.PLS))
				return true;
			else
				return false;
		}
		return false;

	}

	@Override
	public String getDescription() {
		return "Music File";
	}
}
