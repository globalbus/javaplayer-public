package org.player.JavaPlayer.FileUtils;

import java.net.URI;

public class PLSEntry {
	private String title;
	private URI file;
	private int length;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public URI getFile() {
		return file;
	}

	public void setFile(URI file) {
		this.file = file;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
