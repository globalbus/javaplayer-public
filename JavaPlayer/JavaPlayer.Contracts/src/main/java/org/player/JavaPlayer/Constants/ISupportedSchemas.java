package org.player.JavaPlayer.Constants;

public interface ISupportedSchemas {

	String FILE = "file";
	String HTTP = "http";
	String JAR = "jar";

}
