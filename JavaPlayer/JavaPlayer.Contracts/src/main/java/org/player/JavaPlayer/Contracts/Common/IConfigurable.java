package org.player.JavaPlayer.Contracts.Common;

public interface IConfigurable {
    void setConfiguration(IConfiguration configuration);
}
