package org.player.JavaPlayer.Contracts.Common;

import java.util.Map;

public interface IConfiguration {
    Map<Integer, Object> getConfig();

	void init();
}
