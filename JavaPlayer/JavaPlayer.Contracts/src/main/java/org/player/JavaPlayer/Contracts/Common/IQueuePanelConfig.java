package org.player.JavaPlayer.Contracts.Common;

public interface IQueuePanelConfig extends IConfiguration {

	String DOUBLECLICK = "DoubleClick";
	String REMOVE = "remove";
	String MOVEUP = "moveUp";
	String MOVEDOWN = "moveDown";
}
