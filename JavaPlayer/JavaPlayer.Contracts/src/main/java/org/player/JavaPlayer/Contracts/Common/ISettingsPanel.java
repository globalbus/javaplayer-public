package org.player.JavaPlayer.Contracts.Common;

public interface ISettingsPanel {

	void reload();

	void save();

}
