package org.player.JavaPlayer.Contracts.Controllers;

import java.beans.PropertyChangeListener;

import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface IPlayerController {

    void setNewSong(ISongModel songModel);
    ISongModel getCurrentSong();
    
    void play();
    void pause();
    void stop();
    
    void setVolume(double vol);
    double getVolume();
    
    void setPosition(double percent);
	void addPlayerStateListener(PropertyChangeListener listener);
}
