package org.player.JavaPlayer.Contracts.Controllers;

import org.player.JavaPlayer.Contracts.Common.IProperty;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface ISongQueue extends IProperty {
    static final String PUSH = "push";
    static final String FIRST = "first";

    void setCurrentSong(int index);
    void setCurrentSong(ISongModel songModel);
    
    void push(ISongModel model);    
    boolean contains(ISongModel songModel);
    
    ISongModel[] getAll();

    ISongModel getNext();
    ISongModel getPrevious();

    ISongModel getAt(int index);
    int getIndexOf(ISongModel songModel);
    
    int getCount();
	void remove(ISongModel songModel);
	void moveUp(ISongModel songModel);
	void moveDown(ISongModel songModel);
}
