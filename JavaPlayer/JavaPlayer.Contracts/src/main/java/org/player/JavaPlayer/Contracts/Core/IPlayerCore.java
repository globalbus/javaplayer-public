package org.player.JavaPlayer.Contracts.Core;

import org.player.JavaPlayer.Contracts.Common.IConfigurable;
import org.player.JavaPlayer.Contracts.Common.IProperty;

public interface IPlayerCore extends IConfigurable, IProperty{

    String POSITION_CHANGED = "positionChanged";
	String STATE_CHANGED = "stateChanged";
	void setSong(ISongModel songModel);
    ISongModel getSong();

    void play();
    void pause();
    void stop();

    void setVolume(double percent);
    double getVolume();

    boolean setPosition(double percent);   
    int getState();
    
    //void setModel(INowPlayingModel model);
}
