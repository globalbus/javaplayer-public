package org.player.JavaPlayer.Contracts.Core;

import java.awt.Image;
import java.net.URI;
import org.player.JavaPlayer.Contracts.Common.IProperty;

public interface ISongModel extends IProperty {

    URI getURI();
    void setURI(URI uri);

    void setImage(Image image);
    Image getImage();

    void setTrack(String track);
    String getTrack();

    void setGenre(String genre);
    String getGenre();

    void setArtist(String artist);
    String getArtist();

    void setComment(String comment);
    String getComment();

    int getYear();
    void setAlbum(String album);

    void setYear(int year);
    String getAlbum();

    void setTitle(String title);
    String getTitle();

    long getLength();
    void setLength(long length);

    int getBitrate();
    void setBitrate(int bitrate);
}
