package org.player.JavaPlayer.Contracts.Core;

public interface PlayerCoreConfigID {
    static final int SET_SONG =                 13;
    static final int SET_VOLUME =               14;
    static final int SET_POSITION =             15;
    
    static final int POSITION_CHANGED_LISTENER =16;
    
    static final int SET_LOGGING =              17;
    static final int LOGGER_HANDLER =           18;
}
