package org.player.JavaPlayer.Contracts;

import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface ITagReader {

	void getTags(ISongModel song);

}
