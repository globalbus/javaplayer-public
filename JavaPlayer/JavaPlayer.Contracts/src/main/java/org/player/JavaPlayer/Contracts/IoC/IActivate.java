package org.player.JavaPlayer.Contracts.IoC;

import org.player.JavaPlayer.Contracts.IoC.Annotations.Internal;

@Internal
public interface IActivate {
	void activate();
}
