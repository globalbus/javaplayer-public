package org.player.JavaPlayer.Contracts.IoC;

public interface IContainer {

	void putSingleton(Object obj);

	<T> T getSingleton(Class<? extends T> clazz);

	void reload();

	void dispose();

	void savePersistenceClasses();

}
