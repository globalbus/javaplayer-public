package org.player.JavaPlayer.Contracts.IoC;

import org.player.JavaPlayer.Contracts.IoC.Annotations.Internal;

@Internal
public interface IDisposable {
	void deactivate() throws Throwable;
}
