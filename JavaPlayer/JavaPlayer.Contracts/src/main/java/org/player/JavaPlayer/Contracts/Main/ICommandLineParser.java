package org.player.JavaPlayer.Contracts.Main;

import java.io.File;
import java.util.Collection;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface ICommandLineParser {

	Collection<ISongModel> parse(String[] args);

	Collection<ISongModel> parse(File[] files);

	Collection<ISongModel> parse(Collection<ISongModel> models);

}
