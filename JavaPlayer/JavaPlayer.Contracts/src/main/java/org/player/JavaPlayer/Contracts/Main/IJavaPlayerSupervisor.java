package org.player.JavaPlayer.Contracts.Main;

import java.util.Collection;

import org.player.JavaPlayer.Contracts.Controllers.IPlayerController;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface IJavaPlayerSupervisor {
    void play();
    void pause();
    void stop();
    
    void next();
    void previous();
    
    void setVolume(double vol);
    double getVolume();
    
    void setPosition(double percent);
    
    void setPlaylist(ISongModel[] playlist);
    void setPlaylist(Collection<ISongModel> playlist);
    ISongModel[] getPlaylist();
    
    void setCurrentSong(int songModel);
    void setCurrentSong(ISongModel songModel);
    ISongModel getCurrentSong();
	IPlayerController getController();
	void remove(ISongModel newValue);
	void moveUp(ISongModel newValue);
	void moveDown(ISongModel newValue);
}
