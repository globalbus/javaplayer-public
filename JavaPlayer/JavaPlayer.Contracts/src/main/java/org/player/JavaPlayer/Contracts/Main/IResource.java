package org.player.JavaPlayer.Contracts.Main;

import java.awt.Image;

public interface IResource {

	Image getIcon();

}
