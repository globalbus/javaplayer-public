package org.player.JavaPlayer.Contracts.Views.ConfigsID;

public interface ControlPanelConfigID {
    static final int PLAY_BUTTON_ACTION_LISTENER =     1;
    static final int PAUSE_BUTTON_ACTION_LISTENER =    2;
    static final int STOP_BUTTON_ACTION_LISTENER =     3;
    static final int NEXT_BUTTON_ACTION_LISTENER =     4;
    static final int PREV_BUTTON_ACTION_LISTENER =     5;
    
    static final int VOLUME_SLIDER_CHANGE_LISTENER =   6;
    static final int POSITION_SLIDER_CHANGE_LISTENER = 7;
    
    static final int VOLUME_VALUE =                    8;
    static final int POSITION_VALUE =                  9;
    
    static final int CURRENT_STATE =                  10;
}
