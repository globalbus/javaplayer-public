package org.player.JavaPlayer.Contracts.Views.ConfigsID;

public interface MenuBarConfigID {
    static final int MENU_OPEN_ITEM_LISTENER =     13;
    static final int MENU_EXIT_ITEM_LISTENER =     14;
	static final int MENU_OPEN_URL_ITEM_LISTENER = 15;
	static final int MENU_SETTINGS_OPEN_LISTENER = 16;
}
