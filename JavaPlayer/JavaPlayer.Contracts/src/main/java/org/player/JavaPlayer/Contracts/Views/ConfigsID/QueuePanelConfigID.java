package org.player.JavaPlayer.Contracts.Views.ConfigsID;

public interface QueuePanelConfigID {
    static final int SONG_SELECTION_LISTENER =     11;
    static final int SET_CURRENT_SONG =            12;
}
