package org.player.JavaPlayer.Contracts.Views;

import javax.swing.JPanel;
import org.player.JavaPlayer.Contracts.Common.IConfigurable;

public interface IControlPanelManager extends IConfigurable{    
    JPanel getPanel();
    
    double getVolume();
    void setVolume(double vol);
    
    double getPosition();
    void setPosition(double position);
    
    void setState(int state);
    int getState();
    
    boolean isPositionSliderInUse();
    

}

