package org.player.JavaPlayer.Contracts.Views;

import java.awt.Component;

import org.player.JavaPlayer.Contracts.Common.IConfigurable;
import org.player.JavaPlayer.Contracts.Common.ISettingsPanel;

public interface IGradientSettings extends IConfigurable, ISettingsPanel {

	Component getComponent();

	String getName();

}
