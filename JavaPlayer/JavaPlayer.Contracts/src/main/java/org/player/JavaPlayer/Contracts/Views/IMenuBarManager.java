package org.player.JavaPlayer.Contracts.Views;

import javax.swing.JMenuBar;
import org.player.JavaPlayer.Contracts.Common.IConfigurable;

public interface IMenuBarManager extends IConfigurable{
    JMenuBar getMenuBar();
}
