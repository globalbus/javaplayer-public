package org.player.JavaPlayer.Contracts.Views;

import javax.swing.JPanel;
import org.player.JavaPlayer.Contracts.Common.IConfigurable;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

public interface IQueuePanelManager  extends IConfigurable {
 
   
    
    JPanel getPanel();

	void setCurrentSong(ISongModel currentSong);
}
