package org.player.JavaPlayer.Contracts.Views;

import org.player.JavaPlayer.Contracts.Common.IConfigurable;

public interface ISettingsDialog extends IConfigurable {

	void setVisible(boolean visible);

	void reload();

	void saveSettings();

}
