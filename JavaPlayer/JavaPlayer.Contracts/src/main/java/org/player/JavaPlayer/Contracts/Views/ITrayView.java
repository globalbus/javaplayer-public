package org.player.JavaPlayer.Contracts.Views;

import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;

public interface ITrayView {
    void setController(IJavaPlayerSupervisor supervisor);
}
