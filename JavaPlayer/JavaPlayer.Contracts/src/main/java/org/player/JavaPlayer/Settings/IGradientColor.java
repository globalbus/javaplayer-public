package org.player.JavaPlayer.Settings;

import java.awt.Color;

import org.player.JavaPlayer.Contracts.Common.IProperty;

public interface IGradientColor extends IProperty {

	String COLOR1 = "Color1";
	String COLOR2 = "Color2";
	Color DEFAULTCOLOR1=new Color(150, 114, 154);
	Color DEFAULTCOLOR2=new Color(128, 63, 79);

	Color getColor1();

	void setColor1(Color color1);

	Color getColor2();

	void setColor2(Color color2);

}
