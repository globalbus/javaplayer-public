package org.player.JavaPlayer.Settings;

import org.player.JavaPlayer.Contracts.IoC.ISerializable;

public interface IGradientColorSerializable extends ISerializable{

	Integer getColor1();

	void setColor1(Integer color1);

	Integer getColor2();

	void setColor2(Integer color2);

}
