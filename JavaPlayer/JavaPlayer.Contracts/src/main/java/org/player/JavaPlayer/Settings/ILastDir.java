package org.player.JavaPlayer.Settings;

import java.io.File;

public interface ILastDir {

	void setDir(File dir);

	File getDir();

}
