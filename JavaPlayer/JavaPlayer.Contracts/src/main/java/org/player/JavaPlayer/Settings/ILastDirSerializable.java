package org.player.JavaPlayer.Settings;

import org.player.JavaPlayer.Contracts.IoC.ISerializable;

public interface ILastDirSerializable extends ISerializable {

	String getDir();

	void setDir(String dir);

}
