package org.player.JavaPlayer.Settings;

import java.util.List;

import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.ISerializable;

public interface ISongQueueSerializable extends ISerializable {

	void load();

	void setSongs(List<ISongModel> songsList);

	void setCurrentSong(ISongModel currentSong);

}
