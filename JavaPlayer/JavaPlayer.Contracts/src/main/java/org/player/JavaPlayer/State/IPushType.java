package org.player.JavaPlayer.State;

public interface IPushType {
    int PUSH_TYPE_LAST = 0;
    int PUSH_TYPE_FIRST = 1;

    int getPushType();
    void setPushType(int pushType);
}
