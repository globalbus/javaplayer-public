package org.player.JavaPlayer.State;

public interface PlayerState {
    static final int SONG_NOT_SET=0;
    static final int SONG_SET=1;
    static final int PLAYING=2;
    static final int PAUSED=3;
    static final int STOPPED=4;
    static final int FINISHED=5;
}
