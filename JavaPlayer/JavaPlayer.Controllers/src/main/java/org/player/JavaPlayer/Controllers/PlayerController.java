package org.player.JavaPlayer.Controllers;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.player.JavaPlayer.Contracts.Controllers.IPlayerController;
import org.player.JavaPlayer.Contracts.Core.IPlayerCore;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Views.IControlPanelManager;
import org.player.JavaPlayer.State.PlayerState;

public class PlayerController implements IPlayerController, IActivate {

    private IPlayerCore playerCore;
    private ISongModel currentSong;

    //views
    IControlPanelManager controlPanelManager;
    
    @Override
	public void activate(){
    	_create_PlayerPositionChangedListener();
    	
    }
    private void _create_PlayerPositionChangedListener() {
		playerCore.addPropertyChangeListener(IPlayerCore.POSITION_CHANGED, new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				controlPanelManager.setPosition((double) arg0.getNewValue());
			}
			
		});
		
	}
    @Override
    public void addPlayerStateListener(PropertyChangeListener listener){
		playerCore.addPropertyChangeListener(IPlayerCore.STATE_CHANGED, listener);
    }
	//publiczne funkcje
    @Override
    public void setNewSong(ISongModel songModel) {
        currentSong = songModel;
        
        playerCore.setSong(currentSong);
        //queuePanelManager.setCurrentSong(currentSong);
        controlPanelManager.setState(PlayerState.SONG_SET);
    }
    @Override
    public ISongModel getCurrentSong() {
        return currentSong;
    }
    
    
    @Override
    public void play() {
        playerCore.play();
        controlPanelManager.setState(PlayerState.PLAYING);
    }
    @Override
    public void pause() {
        playerCore.pause();
        controlPanelManager.setState(PlayerState.PAUSED);
    }   
    @Override
    public void stop() {
        playerCore.stop();
        controlPanelManager.setState(PlayerState.STOPPED);
    }

    @Override
    public void setVolume(double d) {
        playerCore.setVolume(d);
    }
    @Override
    public double getVolume() {
        return playerCore.getVolume();
    }
    
    @Override
    public void setPosition(double percent) {
        if(currentSong == null) { return; }
        
        //sprawdzenie czy wartość jest prawidłowa
        double correctPercent = percent;
        if(correctPercent < 0) {
            correctPercent = 0;
        } else if (correctPercent > 1) {
            correctPercent = 1.0;
        }
        
        playerCore.setPosition(correctPercent);
    }
    
    @Autowired
    public void setPlayer(IPlayerCore player) {
        this.playerCore = player;

    }
    @Autowired
    public void setControlPanelManager(IControlPanelManager controlPanelManager) {
        this.controlPanelManager = controlPanelManager;
    }

}
