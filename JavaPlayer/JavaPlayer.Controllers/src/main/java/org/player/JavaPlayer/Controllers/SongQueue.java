package org.player.JavaPlayer.Controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.player.JavaPlayer.Contracts.Common.AbstractProperty;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.IDisposable;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Views.IQueuePanelManager;
import org.player.JavaPlayer.Settings.ISongQueueSerializable;

public class SongQueue extends AbstractProperty implements ISongQueue,
		IActivate, IDisposable {

	private IQueuePanelManager queuePanelManager;
	private List<ISongModel> songsList;
	private ISongModel currentSong;
	private ISongQueueSerializable serializable;

	// konstruktor
	public SongQueue() {
		_init();
	}

	// publiczne funkcje
	@Autowired
	public void setQueuePanelManager(IQueuePanelManager queuePanelManager) {
		this.queuePanelManager = queuePanelManager;
	}

	@Autowired
	public void setSongQueueSerializable(ISongQueueSerializable serializable) {
		this.serializable = serializable;
	}

	@Override
	public synchronized void setCurrentSong(int songIndex) {
		setCurrentSong(songsList.get(songIndex));
	}

	@Override
	public synchronized void setCurrentSong(ISongModel songModel) {
		currentSong = songModel;
		queuePanelManager.setCurrentSong(currentSong);
	}

	@Override
	public synchronized ISongModel getNext() {
		int index = songsList.indexOf(currentSong);
		if (index + 1 >= songsList.size()) {
			return null;
		}
		return songsList.get(index + 1);
	}

	@Override
	public synchronized ISongModel getPrevious() {
		int index = songsList.indexOf(currentSong);
		if (index - 1 < 0) {
			return null;
		}
		return songsList.get(index - 1);
	}

	@Override
	public ISongModel[] getAll() {
		return songsList.toArray(new ISongModel[0]);
	}

	@Override
	public synchronized void push(ISongModel songModel) {
		songsList.add(songModel);
		listeners.firePropertyChange(PUSH, null, songModel);
	}

	@Override
	public synchronized boolean contains(ISongModel songModel) {
		return songsList.contains(songModel);
	}

	@Override
	public ISongModel getAt(int index) {
		return songsList.get(index);
	}

	@Override
	public int getIndexOf(ISongModel songModel) {
		return songsList.indexOf(songModel);
	}

	@Override
	public int getCount() {
		return songsList.size();
	}

	// prywatne funkcje
	private void _init() {
		// too much references to index, use ArrayList instead of LinkedList
		songsList = new ArrayList<>();
		currentSong = null;
	}

	@Override
	public synchronized void remove(ISongModel songModel) {
		if (contains(songModel)) {
			songsList.remove(songModel);
			listeners.firePropertyChange(PUSH, null, songModel);
			queuePanelManager.setCurrentSong(currentSong);
		} else
			Logger.getLogger(getClass().getName()).log(Level.ERROR,
					"Trying to remove element with does not exists");
	}
	@Override
	public synchronized void moveDown(ISongModel songModel){
		int index = songsList.indexOf(songModel);
		if(index<songsList.size()){
			songsList.remove(index);
			songsList.add(index+1, songModel);
			queuePanelManager.setCurrentSong(currentSong);
		}
	}
	@Override
	public synchronized void moveUp(ISongModel songModel){
		int index = songsList.indexOf(songModel);
		if(index>0){
			songsList.remove(index);
			songsList.add(index-1, songModel);
			queuePanelManager.setCurrentSong(currentSong);
		}
	}
	@Override
	public void deactivate() throws Throwable {
		serializable.setSongs(songsList);
		serializable.setCurrentSong(currentSong);
	}

	@Override
	public void activate() {
		serializable.load();
	}
}
