package org.player.JavaPlayer.Core;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import org.player.JavaPlayer.Constants.ISupportedSchemas;
import org.player.JavaPlayer.Contracts.Common.AbstractProperty;
import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Core.IPlayerCore;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.Core.PlayerCoreConfigID;
import org.player.JavaPlayer.Contracts.IoC.IDisposable;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Logger.ArgTypes.WarningArgType;
import org.player.JavaPlayer.Logger.JavaPlayerLogger;
import org.player.JavaPlayer.Logger.MsgTypes.Infos;
import org.player.JavaPlayer.Logger.MsgTypes.Warnings;
import org.player.JavaPlayer.State.PlayerState;

import com.sun.glass.ui.Application;

public class PlayerCore extends AbstractProperty implements IPlayerCore,
		IDisposable {

	// ////////////////
	// // SKŁADOWE ////
	// ////////////////
	// składowe statyczne
	final JavaPlayerLogger logger = new JavaPlayerLogger();
	private static final List<String> allowedURIScheme = new ArrayList<>(3);

	// składowe prywatne
	private Media media;
	MediaPlayer player;
	ISongModel currentSong;
	private int currentState;
	private double currentVolume;
	private Duration startPosition;

	// Runnables
	private Runnable playerOnReadyRunnable;
	private Runnable playerOnPausedRunnable;
	private Runnable playerOnStoppedRunnable;
	private Runnable playerOnEndOfMediaRunnable;
	private Runnable playerOnErrorRunnable;

	// listeners list
	List<ChangeListener<Duration>> positionListenersList;

	private ChangeListener<? super Duration> positionListener;
	private Set<Thread> allowedThreads;

	// ////////////////////
	// // KONSTRUKTORY ////
	// ////////////////////
	public PlayerCore() {
		@SuppressWarnings("unused")
		JFXPanel fxPanel = new JFXPanel();
		_init();
	}

	// /////////////////////////
	// // PUBLICZNE FUNKCJE ////
	// /////////////////////////
	@Override
	public void setSong(ISongModel songModel) {
		URI songURI = songModel.getURI();
		String scheme = songURI.getScheme().toLowerCase();
		if (!allowedURIScheme.contains(scheme)) {
			logger.logWarning(Warnings.NOT_ALLOWED_SCHEME, scheme,
					WarningArgType.SCHEME);
			return;
		}

		synchronized (this) {
			currentSong = songModel;
			media = new Media(songURI.toString());
			// jeżeli muzyka gra lub jest włączona pause'a
			if(getState()==PlayerState.SONG_SET){
				try {
					wait(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (getState() == PlayerState.PLAYING
					|| getState() == PlayerState.PAUSED) {
				stop();
				try {
					wait(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			_setState(PlayerState.SONG_SET);
		}




	}

	@Override
	public ISongModel getSong() {
		return currentSong;
	}

	@Override
	public synchronized void play() {
		switch (currentState) {
		case PlayerState.SONG_NOT_SET:
			logger.logWarning(Warnings.SONG_NOT_SET, "play()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.SONG_SET:
			_initNewSong();
			break;

		case PlayerState.PLAYING:
			logger.logWarning(Warnings.ALREADY_PLAYING, "play()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.PAUSED:
			_playSong();
			break;

		case PlayerState.STOPPED:
			_playSong();
			break;
		default:
			break;
		}

	}

	@Override
	public synchronized void pause() {
		switch (currentState) {
		case PlayerState.SONG_NOT_SET:
			logger.logWarning(Warnings.SONG_NOT_SET, "pause()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.SONG_SET:
			logger.logWarning(Warnings.NOT_PLAYING, "pause()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.PLAYING:
			startPosition = player.getCurrentTime();
			player.pause();
			break;

		case PlayerState.PAUSED:
			logger.logWarning(Warnings.ALREADY_PAUSED, "pause()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.STOPPED:
			logger.logWarning(Warnings.CANNOT_BE_PAUSED, "STOPPED",
					WarningArgType.STATE);
			break;
		default:
			break;
		}

	}

	@Override
	public synchronized void stop() {
		switch (currentState) {
		case PlayerState.SONG_NOT_SET:
			logger.logWarning(Warnings.SONG_NOT_SET, "stop()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.SONG_SET:
			logger.logWarning(Warnings.NOT_PLAYING, "stop()",
					WarningArgType.COMMAND);
			break;

		case PlayerState.PLAYING:
			player.stop();
			break;

		case PlayerState.PAUSED:
			player.stop();
			break;

		case PlayerState.STOPPED:
			logger.logWarning(Warnings.ALREADY_STOPPED, "stop()",
					WarningArgType.COMMAND);
			break;
		default:
			break;
		}

	}

	@Override
	public void setVolume(double percent) {
		currentVolume = _getProperPercent(percent);

		if (player != null) {
			player.setVolume(currentVolume);
		}
		logger.logInfo(currentVolume + "", Infos.VOLUME);
	}

	@Override
	public double getVolume() {
		return player.getVolume();
	}

	@Override
	public boolean setPosition(double percent) {
		boolean conditions = _checkConditionsForSettingPosition1();
		if (!conditions) {
			return false;
		}

		double durationInMs = media.getDuration().toMillis();
		Double value = _getProperPercent(percent);

		value = durationInMs * value; // poszukiwana pozycja

		return setPosition(Duration.millis(value));
	}

	private synchronized boolean setPosition(Duration duration) {
		if (player == null) {
			return false;
		}

		boolean conditions = _checkConditionsForSettingPosition2(duration);
		if (!conditions) {
			return false;
		}

		if (currentState == PlayerState.PLAYING) {
			player.seek(duration);
		} else {
			startPosition = duration;
		}

		return true;

	}

	@Override
	public synchronized int getState() {
		return currentState;

	}

	public void loggingPlayer(boolean logging) {
		logger.setLogging(logging);
	}

	public boolean isLogging() {
		return logger.isLogging();
	}

	public void addLoggerHandler(Handler handler) {
		logger.addListener(handler);
	}

	public void removeHandler(Handler handler) {
		logger.removeListener(handler);
	}

	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();

		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}

	// ////////////////////////
	// // PRYWATNE FUNKCJE ////
	// ////////////////////////
	private void _init() {
		allowedURIScheme.add(ISupportedSchemas.FILE);
		allowedURIScheme.add(ISupportedSchemas.HTTP);
		allowedURIScheme.add(ISupportedSchemas.JAR);

		currentSong = null;
		currentState = PlayerState.SONG_NOT_SET;
		currentVolume = 0.5;

		positionListenersList = new LinkedList<>();

		_createRunnables();
		allowedThreads=new HashSet<>();
	}

	private synchronized void _initNewSong() {
		//ugly workaround
		if(player==null)
			allowedThreads.addAll(Thread.getAllStackTraces().keySet());
		Set<Thread> toKill = Thread.getAllStackTraces().keySet();
		toKill.removeAll(allowedThreads);
		for(Thread i :toKill)
				i.interrupt();
		// nowa piosenka == nowy obiekt MediaPlayer
		player = new MediaPlayer(media);
		player.setVolume(currentVolume);
		startPosition = Duration.millis(0);
		System.gc();
		_addPlayerLifeCycleListeners(); // uruchamia song'a poprzez onReady()
		_addPlayerPositionListeners(); // dodaje słuchaczy zmiany pozycji
	}

	private void _addPlayerLifeCycleListeners() {
		player.setOnReady(playerOnReadyRunnable);
		player.setOnPaused(playerOnPausedRunnable);
		player.setOnStopped(playerOnStoppedRunnable);
		player.setOnEndOfMedia(playerOnEndOfMediaRunnable);
		player.setOnError(playerOnErrorRunnable);
	}

	private void _addPlayerPositionListeners() {
		ReadOnlyObjectProperty<Duration> positionProperty = player
				.currentTimeProperty();
		positionProperty.addListener(positionListener);
	}

	synchronized void _playSong() {
		player.seek(startPosition);

		player.play();
		_setState(PlayerState.PLAYING);
	}

	void _setState(int newState) {
		int oldValue = currentState;
		synchronized (this) {
			logger.logInfo(currentState, newState);
			currentState = newState;
			notifyAll();
		}
		listeners.firePropertyChange(IPlayerCore.STATE_CHANGED, oldValue,
				newState);
	}

	// Runnables
	private void _createRunnables() {
		_createPlayerOnReadyRunnable();
		_createPlayerOnPausedRunnable();
		_createPlayerOnStoppedRunnable();
		_createPlayerOnEndOfMediaRunnable();
		_createPlayerOnErrorRunnable();
		_createPlayerPositionChangedListener();
	}

	private void _createPlayerOnReadyRunnable() {
		playerOnReadyRunnable = new Runnable() {
			@Override
			public void run() {
				_playSong();
			}
		};
	}

	private void _createPlayerOnPausedRunnable() {
		playerOnPausedRunnable = new Runnable() {
			@Override
			public void run() {
				_setState(PlayerState.PAUSED);
			}
		};
	}

	private void _createPlayerOnStoppedRunnable() {
		playerOnStoppedRunnable = new Runnable() {
			@Override
			public void run() {
				_setState(PlayerState.STOPPED);
			}
		};
	}

	private void _createPlayerOnEndOfMediaRunnable() {
		playerOnEndOfMediaRunnable = new Runnable() {
			@Override
			public void run() {
				_setState(PlayerState.FINISHED);
				
			}
		};
	}

	private void _createPlayerOnErrorRunnable() {
		playerOnErrorRunnable = new Runnable() {
			@Override
			public void run() {
				logger.logSevere(player.getError());
			}
		};
	}

	private void _createPlayerPositionChangedListener() {
		ChangeListener<Duration> playerPositionChangedListener = new ChangeListener<Duration>() {
			@Override
			public void changed(ObservableValue<? extends Duration> ov,
					Duration oldValue, Duration newValue) {
				double currentPosition = newValue.toSeconds();
				double percent = currentPosition / currentSong.getLength();
				listeners.firePropertyChange(IPlayerCore.POSITION_CHANGED,
						null, percent);
			}
		};
		positionListener = playerPositionChangedListener;
	}

	// helper
	private double _getProperPercent(double percent) {
		double value = percent;
		if (value > 1.0) {
			value = 1.0;
		} else if (value < 0) {
			value = 0.0;
		}

		return value;
	}

	private boolean _checkConditionsForSettingPosition1() {
		if (currentState == PlayerState.SONG_NOT_SET) {
			logger.logWarning(Warnings.UNABLE_TO_SET_POSITION,
					Integer.toString(currentState), WarningArgType.STATE);
			return false;
		}

		Double durationInMs = media.getDuration().toMillis(); // możliwy NaN lub
		// POSITIVE_INFINITY
		boolean badDuration = durationInMs == Double.NaN
				|| durationInMs == Double.POSITIVE_INFINITY;
		if (badDuration) {
			logger.logWarning(Warnings.UNABLE_TO_SET_POSITION,
					durationInMs.toString(), WarningArgType.DURATION);
			return false;
		}

		return true;
	}

	private boolean _checkConditionsForSettingPosition2(Duration duration) {
		boolean unknownDuration = duration == Duration.UNKNOWN;
		boolean badState = currentState == PlayerState.SONG_NOT_SET
				|| currentState == PlayerState.STOPPED;

		// na potrzeby loggera rozbite na 2 if'y
		if (badState) {
			logger.logWarning(Warnings.UNABLE_TO_SET_POSITION,
					Integer.toString(currentState), WarningArgType.STATE);
			return false;

		} else if (unknownDuration) {
			logger.logWarning(Warnings.UNABLE_TO_SET_POSITION,
					duration.toString(), WarningArgType.DURATION);
			return false;
		}

		return true;
	}

	private void _makeSetting(int id, Object config) {
		switch (id) {
		case PlayerCoreConfigID.SET_SONG:
			setSong((ISongModel) config);
			break;

		case PlayerCoreConfigID.SET_VOLUME:
			setVolume((double) config);
			break;

		case PlayerCoreConfigID.SET_POSITION:
			Class<?> clazz = config.getClass();

			if (clazz == double.class || clazz == Double.class) {
				setPosition((double) config);

			} else if (clazz == Duration.class) {
				setPosition((Duration) config);
			}

			break;

		case PlayerCoreConfigID.SET_LOGGING:
			loggingPlayer((boolean) config);
			break;

		case PlayerCoreConfigID.LOGGER_HANDLER:
			addLoggerHandler((Handler) config);
			break;

		default:
			Logger.getLogger(Constants.APPNAME).log(
					Level.WARNING,
					"Pominięto nieznaną konfigurację PlayerCore" + "\n\tID: "
							+ id + "\n\tConfig: " + config.getClass());
		}
	}

	@Override
	public void deactivate() throws Throwable {
		this.finalize();
	}
}
