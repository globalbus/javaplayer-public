package org.player.JavaPlayer.Logger.ArgTypes;

public enum WarningArgType {
    COMMAND,
    SCHEME,
    STATE,
    DURATION,
    UNCATEGORIZED
}
