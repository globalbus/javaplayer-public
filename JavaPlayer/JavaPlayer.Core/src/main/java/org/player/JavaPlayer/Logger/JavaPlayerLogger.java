package org.player.JavaPlayer.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.MediaException;
import org.player.JavaPlayer.Core.PlayerCore;
import org.player.JavaPlayer.Logger.ArgTypes.WarningArgType;
import org.player.JavaPlayer.Logger.MsgTypes.Infos;
import org.player.JavaPlayer.Logger.MsgTypes.Warnings;
import org.player.JavaPlayer.State.PlayerState;

public class JavaPlayerLogger {
	// składowe statyczne
	private static final Logger logger = Logger.getLogger(PlayerCore.class.getName());
	private static final Map<Warnings, String> warnings = new HashMap<>(7);
	private static final Map<Integer, String> info = new HashMap<>(6);

	// składowe prywatne
	private boolean logging;

	// konstruktor
	public JavaPlayerLogger() {
		this(true);
	}

	public JavaPlayerLogger(boolean logging) {
		_init();
		this.logging = logging;
	}

	// publiczne funkcje
	public void logWarning(Warnings war) {
		logWarning(war, "", WarningArgType.UNCATEGORIZED);
	}

	public void logWarning(Warnings war, String arg, WarningArgType type) {
		if (!logging) { // sprawdzenie czy zapisać loga
			return;
		}

		String msg = warnings.get(war);
		switch (type) {
		case COMMAND:
			msg = msg.concat(", cannot execute command: " + arg);
			break;

		case SCHEME:
			msg = msg.concat(", scheme trying to use: " + arg);
			break;

		case STATE:
			msg = msg.concat(", current state: " + arg);
			break;

		case DURATION:
			msg = msg.concat(", duration to set: " + arg);
			break;

		case UNCATEGORIZED:
			msg = msg.concat(arg);
			break;
		default:
			break;
		}

		_log(Level.WARNING, msg);
	}

	public void logInfo(int currentState, int newState) {
		String msg = info.get(newState);
		msg += ", [" + currentState + "] => [" + newState + "]";

		logInfo(msg, Infos.STATE);
	}

	public void logInfo(String msg, Infos type) {
		switch (type) {
		case STATE:
			_log(Level.INFO, msg);
			break;

		case VOLUME:
			_log(Level.INFO, "Volume set to: " + msg);
			break;
		default:
			break;
		}
	}

	public void logSevere(MediaException err) {
		_log(Level.SEVERE, err.toString());
	}

	public boolean isLogging() {
		return logging;
	}

	public void setLogging(boolean logging) {
		this.logging = logging;
	}

	public void addListener(Handler handler) {
		logger.addHandler(handler);
	}

	public void removeListener(Handler handler) {
		logger.removeHandler(handler);
	}

	// prywatne funkcje
	private void _init() {
		logger.setLevel(Level.ALL);

		warnings.put(Warnings.NOT_ALLOWED_SCHEME,       "Unrecognized scheme");
		warnings.put(Warnings.SONG_NOT_SET,             "Song is not set");
		warnings.put(Warnings.ALREADY_PLAYING,          "Audio Player is already playing a song");
		warnings.put(Warnings.NOT_PLAYING,              "Song is set, but player is not playing a song");
		warnings.put(Warnings.ALREADY_PAUSED,           "Song is already paused");
		warnings.put(Warnings.CANNOT_BE_PAUSED,         "Song cannot be paused in current state");
		warnings.put(Warnings.ALREADY_STOPPED,          "Song is already stopped");
		warnings.put(Warnings.UNABLE_TO_SET_POSITION,   "Unable to set position");

		info.put(PlayerState.SONG_NOT_SET, "JavaPlayer started");
		info.put(PlayerState.SONG_SET, "Song set successfully");
		info.put(PlayerState.PLAYING, "JavaPlayer is playing a song");
		info.put(PlayerState.PAUSED, "Song paused");
		info.put(PlayerState.STOPPED, "Song stopped");
		info.put(PlayerState.FINISHED, "JavaPlayer finished playing a song");
	}

	private void _log(Level lv, String msg) {
		logger.log(lv, msg);
	}
}
