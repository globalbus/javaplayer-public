package org.player.JavaPlayer.Logger.MsgTypes;

public enum Warnings {
    NOT_ALLOWED_SCHEME,
    SONG_NOT_SET,
    ALREADY_PLAYING,
    NOT_PLAYING,
    ALREADY_PAUSED,
    CANNOT_BE_PAUSED,
    ALREADY_STOPPED,
    UNABLE_TO_SET_POSITION
}
