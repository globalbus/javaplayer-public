package org.player.JavaPlayer.IoC;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.IContainer;
import org.player.JavaPlayer.Contracts.IoC.IDisposable;
import org.player.JavaPlayer.Contracts.IoC.ISerializable;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Internal;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Controllers.PlayerController;
import org.player.JavaPlayer.Controllers.SongQueue;
import org.player.JavaPlayer.Core.PlayerCore;
import org.player.JavaPlayer.Core.Resources;
import org.player.JavaPlayer.Main.MainManager;
import org.player.JavaPlayer.Main.ComponentsConfigs.ControlPanelConfig;
import org.player.JavaPlayer.Main.ComponentsConfigs.GradientPanelConfig;
import org.player.JavaPlayer.Main.ComponentsConfigs.MenuBarConfig;
import org.player.JavaPlayer.Main.ComponentsConfigs.QueuePanelConfig;
import org.player.JavaPlayer.Main.ComponentsConfigs.SettingsDialogConfig;
import org.player.JavaPlayer.Main.Settings.GradientColor;
import org.player.JavaPlayer.Main.Settings.GradientColorSerializable;
import org.player.JavaPlayer.Main.Settings.LastDir;
import org.player.JavaPlayer.Main.Settings.LastDirSerializable;
import org.player.JavaPlayer.Main.Settings.SonqQueueSerializable;
import org.player.JavaPlayer.Main.Supervisor.JavaPlayerSupervisor;
import org.player.JavaPlayer.Main.Utilities.CommandLineParser;
import org.player.JavaPlayer.Views.FrameManager;
import org.player.JavaPlayer.Views.MenuBar.MenuBarManager;
import org.player.JavaPlayer.Views.Panels.ControlPanel.ControlPanelManager;
import org.player.JavaPlayer.Views.Panels.QueuePanel.QueuePanelManager;
import org.player.JavaPlayer.Views.Settings.GradientSettingsPanel;
import org.player.JavaPlayer.Views.Settings.SettingsDialog;
import org.player.JavaPlayer.Views.Tray.TrayView;

public class PseudoContainer implements IContainer {

	private static final String CONFIGFILE = "config.xml";
	private Map<String, Object> configs = new HashMap<>();
	private Map<String, Object> resolved = new HashMap<>();
	public static IContainer Instance;

	static {
		Instance = new PseudoContainer();
	}

	// konstruktor
	private PseudoContainer() {
		_register();
		loadPersistenceClasses();
		_solveDependencies();
	}

	/**
	 * Some of classes in application may need to do additional actions before
	 * closing. This method should be called instead of calling System.exit()
	 * directly.
	 */
	@Override
	public void dispose() {
		for (Object obj : configs.values()) {
			if (obj instanceof IDisposable)
				try {
					((IDisposable) obj).deactivate();
				} catch (Throwable e) {
					e.printStackTrace();
				}
		}
		savePersistenceClasses();
		System.exit(0);
	}

	/**
	 * load all beans from XML file.
	 * 
	 */
	@SuppressWarnings("unchecked")
	private void loadPersistenceClasses() {
		Collection<Object> temp = null;
		try (XMLDecoder reader = new XMLDecoder(new BufferedInputStream(
				new FileInputStream(CONFIGFILE)))) {
			temp = (Collection<Object>) reader.readObject();
		} catch (FileNotFoundException e) {
			Logger.getLogger(Constants.APPNAME).log(Level.WARNING,
					"Persistence file not found");
		}
		if (temp != null)
			for (Object i : temp) {
				putSingleton(i);
			}
	}

	/**
	 * Save all classes which implement @see ISerializable interface to XML
	 * 
	 */
	@Override
	public void savePersistenceClasses() {
		Collection<Object> temp = new LinkedList<>();
		for (Object obj : configs.values())
			if (obj instanceof ISerializable)
				temp.add(obj);
		try (XMLEncoder writer = new XMLEncoder(new BufferedOutputStream(
				new FileOutputStream(CONFIGFILE)))) {
			writer.writeObject(temp);
		} catch (FileNotFoundException e) {
			Logger.getLogger(Constants.APPNAME).log(Level.WARNING,
					"Persistence file not found");
		}
	}

	/**
	 * Refresh container state. Used in unit tests
	 * 
	 */
	@Override
	public void reload() {
		for (Object obj : configs.values()) {
			if (obj instanceof IDisposable)
				try {
					((IDisposable) obj).deactivate();
				} catch (Throwable e) {
					e.printStackTrace();
				}
		}
		Instance = new PseudoContainer();
		System.gc();
	}

	/**
	 * @param object
	 *            to register
	 */
	@Override
	public void putSingleton(Object obj) {
		Class<?> clazz = obj.getClass();
		if (!clazz.isInterface()) {
			for (Class<?> item : clazz.getInterfaces()) {
				if (!item.isAnnotationPresent(Internal.class)) // internal
																// interfaces
																// should not be
																// registered
					configs.put(item.getName(), obj);
			}
		}
	}

	/**
	 * getting registered object by providing interface class
	 * 
	 * @return registered object or null if nothing found
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <T> T getSingleton(Class<? extends T> clazz) {
		T result = null;
		try {
			if (clazz.isInterface()) {
				result = (T) configs.get(clazz.getName());
			}

		} catch (ClassCastException e) {
			// this simply can't occurs, but...
			Logger.getLogger(Constants.APPNAME).log(Level.SEVERE,
					"Error in Container");
		}
		return result;
	}

	private void _register() {
		// workaround to register itself and provide container functionality via
		// @see Autowired
		putSingleton(this);

		putSingleton(new PlayerCore());
		putSingleton(new PlayerController());
		putSingleton(new SongQueue());
		putSingleton(new JavaPlayerSupervisor());

		putSingleton(new FrameManager());

		putSingleton(new QueuePanelManager());
		putSingleton(new ControlPanelManager());
		putSingleton(new MenuBarManager());

		putSingleton(new CommandLineParser());
		putSingleton(new Resources());
		putSingleton(new TrayView());
		putSingleton(new MainManager());
		putSingleton(new ControlPanelConfig());
		putSingleton(new MenuBarConfig());
		putSingleton(new QueuePanelConfig());

		putSingleton(new LastDirSerializable());
		putSingleton(new LastDir());
		putSingleton(new GradientColor());
		putSingleton(new SettingsDialog());
		putSingleton(new GradientSettingsPanel());
		putSingleton(new SettingsDialogConfig());
		putSingleton(new GradientPanelConfig());
		putSingleton(new GradientColorSerializable());
		putSingleton(new SonqQueueSerializable());
	}

	/**
	 * Solve all setter dependencies with
	 * 
	 * @see Autowired annotation for registered dependencies
	 */
	private void _solveDependencies() {
		HashMap<String, Object> unresolved = new HashMap<>(configs);
		for (Entry<String, Object> unresolvedEntry : unresolved.entrySet()) {
			Method[] methods = unresolvedEntry.getValue().getClass()
					.getDeclaredMethods();
			Collection<Method> annotatedMethods = new LinkedList<>();
			for (Method method : methods)
				if (method.isAnnotationPresent(Autowired.class))
					annotatedMethods.add(method);
			if (annotatedMethods.size() == 0)
				resolved.put(unresolvedEntry.getKey(),
						unresolvedEntry.getValue());
			else {
				int i = 0;
				for (Method method : annotatedMethods) {
					i++;
					Class<?>[] params = method.getParameterTypes();
					if (params.length == 1) {
						try {
							method.invoke(unresolvedEntry.getValue(),
									getSingleton(params[0]));
							if (i == annotatedMethods.size())
								resolved.put(unresolvedEntry.getKey(),
										unresolvedEntry.getValue());
						} catch (IllegalAccessException
								| IllegalArgumentException
								| InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				}
			}

		}
		for (Object obj : resolved.values())
			if (obj instanceof IActivate)
				((IActivate) obj).activate();
	}
}
