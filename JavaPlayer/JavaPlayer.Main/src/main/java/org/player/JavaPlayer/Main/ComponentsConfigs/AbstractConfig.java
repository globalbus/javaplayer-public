package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.util.HashMap;
import java.util.Map;

import org.player.JavaPlayer.Contracts.Common.IConfiguration;

public abstract class AbstractConfig implements IConfiguration{
    protected Map<Integer, Object> configuration;
    public AbstractConfig(){
    	configuration= new HashMap<>();
    }
    
    @Override
    public Map<Integer, Object> getConfig() {
        return configuration;
    }
}
