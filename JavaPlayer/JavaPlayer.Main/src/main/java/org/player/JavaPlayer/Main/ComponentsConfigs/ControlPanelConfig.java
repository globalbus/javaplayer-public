package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.player.JavaPlayer.Contracts.Common.IControlPanelConfig;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.ControlPanelConfigID;
import org.player.JavaPlayer.State.PlayerState;

public class ControlPanelConfig extends AbstractConfig implements IControlPanelConfig, IActivate{

    IJavaPlayerSupervisor supervisor;
    @Autowired
    public void setSupervisor(IJavaPlayerSupervisor supervisor) {
		this.supervisor = supervisor;
	}
    @Override
	public void activate(){
        _init();
    }
	//control panel listeners
    private ActionListener playButtonActionListener;
    private ActionListener pauseButtonActionListener;
    private ActionListener stopButtonActionListener;
    
    private ActionListener nextButtonActionListener;
    private ActionListener prevButtonActionListener;
    
    private PropertyChangeListener volumeSliderChangeListener;
    private PropertyChangeListener positionSliderChangeListener;
        
    //prywatne funkcje
    private void _init() {
        _createControlPanelListeners();
        configuration.put(ControlPanelConfigID.PLAY_BUTTON_ACTION_LISTENER, playButtonActionListener);
        configuration.put(ControlPanelConfigID.PAUSE_BUTTON_ACTION_LISTENER, pauseButtonActionListener);
        configuration.put(ControlPanelConfigID.STOP_BUTTON_ACTION_LISTENER, stopButtonActionListener);
        configuration.put(ControlPanelConfigID.NEXT_BUTTON_ACTION_LISTENER, nextButtonActionListener);
        configuration.put(ControlPanelConfigID.PREV_BUTTON_ACTION_LISTENER, prevButtonActionListener);
        
        configuration.put(ControlPanelConfigID.VOLUME_SLIDER_CHANGE_LISTENER, volumeSliderChangeListener);
        configuration.put(ControlPanelConfigID.POSITION_SLIDER_CHANGE_LISTENER, positionSliderChangeListener);
        
        configuration.put(ControlPanelConfigID.CURRENT_STATE, PlayerState.SONG_NOT_SET);
    }
    
    private void _createControlPanelListeners() {
        _create_PlayButtonActionListener();
        _create_PauseButtonActionListener();
        _create_StopButtonActionListener();
        _create_NextButtonActionListener();
        _create_PrevButtonActionListener();
        
        _create_VolumeSliderChangeListener();
        _create_PositionSliderChangeListener();
    }     
    
    private void _create_PlayButtonActionListener() {
        playButtonActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                supervisor.play();
            }
        };
    }
    private void _create_PauseButtonActionListener() {
        pauseButtonActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                supervisor.pause();
            }
        };
    }
    private void _create_StopButtonActionListener() {
        stopButtonActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                supervisor.stop();
            }
        };
    }
    private void _create_NextButtonActionListener() {
        nextButtonActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                supervisor.next();
            }
        };
    }
    private void _create_PrevButtonActionListener() {
        prevButtonActionListener = new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                supervisor.previous();
            }
        };
    }
    
    private void _create_VolumeSliderChangeListener() {
        volumeSliderChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                double newVol = (double)evt.getNewValue();
                supervisor.setVolume(newVol);
            }
        };
    }
    private void _create_PositionSliderChangeListener() {
        positionSliderChangeListener = new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                double percent = (double) evt.getNewValue();
                supervisor.setPosition(percent);
            }
        };
    }
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
}
