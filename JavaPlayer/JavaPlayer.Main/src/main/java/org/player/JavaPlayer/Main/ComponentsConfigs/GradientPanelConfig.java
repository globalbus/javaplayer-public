package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JColorChooser;

import org.player.JavaPlayer.Contracts.Common.IGradientPanelConfig;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.GradientPanelConfigID;


public class GradientPanelConfig extends AbstractConfig implements
		IGradientPanelConfig {
	private ActionListener changeColorButtonListener;

	public GradientPanelConfig() {
		_init();
	}

	private void _init() {
		_create_change_color_button_listener();
		configuration.put(GradientPanelConfigID.CHANGE_COLOR_BUTTON_LISTENER, changeColorButtonListener);
	}

	private void _create_change_color_button_listener() {
		changeColorButtonListener = new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JButton source = (JButton) arg0.getSource();
				Color color = JColorChooser.showDialog(null,
						arg0.getActionCommand(), source.getBackground());
				if (color == null)
					return;
				source.setBackground(color);
			}
		};
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

}
