package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import org.player.JavaPlayer.Contracts.Common.IMenuBarConfig;
import org.player.JavaPlayer.Contracts.IoC.IContainer;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Contracts.Views.ISettingsDialog;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.MenuBarConfigID;
import org.player.JavaPlayer.FileUtils.MusicFileFilter;
import org.player.JavaPlayer.Settings.ILastDir;

public class MenuBarConfig extends AbstractConfig implements IMenuBarConfig {

	IJavaPlayerSupervisor supervisor;
	ICommandLineParser parser;
	ILastDir dir;
	IContainer container;
	ISettingsDialog dialog;

	@Autowired
	public void setSupervisor(IJavaPlayerSupervisor supervisor) {
		this.supervisor = supervisor;
	}

	@Autowired
	public void setParser(ICommandLineParser parser) {
		this.parser = parser;
	}

	@Autowired
	public void setContainer(IContainer container){
		this.container = container;
	}
	@Autowired
	public void setLastDir(ILastDir dir) {
		this.dir = dir;
	}
	@Autowired
	public void setSettingsDialog(ISettingsDialog dialog){
		this.dialog=dialog;
	}

	private ActionListener menuOpenItemListener;
	private ActionListener menuExitItemListener;

	JFileChooser fileChooser;
	private ActionListener menuOpenURLItemListener;
	private ActionListener menuSettingsOpenListener;


	@Override
	public void init() {
		_create_MenuItemsListeners();

		configuration.put(MenuBarConfigID.MENU_OPEN_ITEM_LISTENER,
				menuOpenItemListener);
		configuration.put(MenuBarConfigID.MENU_OPEN_URL_ITEM_LISTENER,
				menuOpenURLItemListener);
		configuration.put(MenuBarConfigID.MENU_EXIT_ITEM_LISTENER,
				menuExitItemListener);
		configuration.put(MenuBarConfigID.MENU_SETTINGS_OPEN_LISTENER,
				menuSettingsOpenListener);
	}

	private void _create_MenuItemsListeners() {
		_create_MenuOpenItemListener();
		_create_MenuOpenURLItemListener();
		_create_MenuExitItemListener();
		_create_MenuSettingsOpenListener();
	}

	private void _create_MenuOpenItemListener() {
		_initFileChooser();

		menuOpenItemListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// Show open file dialog
				int response = fileChooser.showOpenDialog(null);
				if (response == JFileChooser.APPROVE_OPTION) {
					File[] file = fileChooser.getSelectedFiles();

					supervisor.setPlaylist(parser.parse(file));
					dir.setDir(fileChooser.getCurrentDirectory());
				}
			}
		};
	}

	private void _create_MenuOpenURLItemListener() {
		menuOpenURLItemListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String ret = JOptionPane.showInputDialog("Wprowadź adres");
				supervisor.setPlaylist(parser.parse(new String[] { ret }));

			}
		};
	}

	private void _create_MenuExitItemListener() {
		menuExitItemListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				container.dispose();
			}
		};
	}
	private void _create_MenuSettingsOpenListener(){
		menuSettingsOpenListener = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(true);
				
			}
			
		};
	}
	private void _initFileChooser() {
		fileChooser = new JFileChooser();
		FileFilter filter = new MusicFileFilter();
		if (dir.getDir() != null) {
				fileChooser.setCurrentDirectory(dir.getDir());
		}
		fileChooser.setFileFilter(filter);
		fileChooser.setMultiSelectionEnabled(true);
	}
}
