package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.player.JavaPlayer.Contracts.Common.IQueuePanelConfig;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.QueuePanelConfigID;

public class QueuePanelConfig extends AbstractConfig implements IActivate,
		IQueuePanelConfig {
	IJavaPlayerSupervisor supervisor;

	@Autowired
	public void setSupervisor(IJavaPlayerSupervisor supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public void activate() {
		_init();
	}

	// listeners
	private PropertyChangeListener songSelectionListener;

	// prywatne funkcje
	private void _init() {
		_create_SongSelectionListener();

		configuration.put(QueuePanelConfigID.SONG_SELECTION_LISTENER,
				songSelectionListener);
	}

	private void _create_SongSelectionListener() {
		songSelectionListener = new PropertyChangeListener() {

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				switch (evt.getPropertyName()) {
				case IQueuePanelConfig.DOUBLECLICK:
					supervisor.setCurrentSong((ISongModel) evt.getNewValue());
					break;
				case IQueuePanelConfig.REMOVE:
					supervisor.remove((ISongModel) evt.getNewValue());
					break;
				case IQueuePanelConfig.MOVEUP:
					supervisor.moveUp((ISongModel) evt.getNewValue());
					break;
				case IQueuePanelConfig.MOVEDOWN:
					supervisor.moveDown((ISongModel) evt.getNewValue());
					break;
				default:
					break;
				}
			}
		};
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub

	}
}
