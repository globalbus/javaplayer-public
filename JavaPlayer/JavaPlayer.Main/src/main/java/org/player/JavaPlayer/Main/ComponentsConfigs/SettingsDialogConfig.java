package org.player.JavaPlayer.Main.ComponentsConfigs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.player.JavaPlayer.Contracts.Common.ISettingsDialogConfig;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Views.ISettingsDialog;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.SettingsDialogConfigID;

public class SettingsDialogConfig extends AbstractConfig implements ISettingsDialogConfig{
	ISettingsDialog dialog;
	private ActionListener cancelListener;
	private ActionListener okListener;

	@Autowired
	public void setSettingsDialog(ISettingsDialog dialog){
		this.dialog=dialog;
	}

	public SettingsDialogConfig(){
		_init();
	}

	private void _init() {
		_create_ok_listener();
		_create_cancel_listener();
		configuration.put(SettingsDialogConfigID.OK_LISTENER, okListener);
		configuration.put(SettingsDialogConfigID.CANCEL_LISTENER, cancelListener);
	}

	private void _create_cancel_listener() {
		cancelListener = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				dialog.reload();
			}
			
		};
	}

	private void _create_ok_listener() {
		okListener = new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
				dialog.saveSettings();
			}
			
		};
		
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

}
