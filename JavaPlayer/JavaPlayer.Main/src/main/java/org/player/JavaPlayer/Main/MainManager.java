package org.player.JavaPlayer.Main;

import javax.swing.SwingUtilities;

import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.IMainManager;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;


/** TODO
 * this class should be deleted
 *
 */
public class MainManager implements IMainManager{
    
    IFrameManager frame;
    @Autowired
	public void setFrame(IFrameManager frame) {
		this.frame = frame;
	}
    public MainManager(){
    	
    }
    @Override
    public void startUp(){
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame.setVisible(true);
            }
        });
    }
}
