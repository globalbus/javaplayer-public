package org.player.JavaPlayer.Main.Settings;

import java.awt.Color;

import org.player.JavaPlayer.Contracts.Common.AbstractProperty;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Settings.IGradientColor;
import org.player.JavaPlayer.Settings.IGradientColorSerializable;

public class GradientColor extends AbstractProperty implements IGradientColor {
	private Color color1;
	private Color color2;
	private IGradientColorSerializable colorPersistence;

	@Autowired
	public void setGradientColor(IGradientColorSerializable colorPersistence) {
		this.colorPersistence = colorPersistence;
		if(colorPersistence.getColor1()!=null)
			setColor1(new Color(colorPersistence.getColor1()));
		if(colorPersistence.getColor2()!=null)
			setColor2(new Color(colorPersistence.getColor2()));
	}

	public GradientColor() {
		color1 = IGradientColor.DEFAULTCOLOR1;
		color2 = IGradientColor.DEFAULTCOLOR2;
	}

	@Override
	public Color getColor1() {
		return color1;
	}

	@Override
	public void setColor1(Color color1) {
		Color old = this.color1;
		this.color1 = color1;
		listeners.firePropertyChange(IGradientColor.COLOR1, old, color1);
		colorPersistence.setColor1(color1.getRGB());
	}

	@Override
	public Color getColor2() {
		return color2;
	}

	@Override
	public void setColor2(Color color2) {
		Color old = this.color2;
		this.color2 = color2;
		listeners.firePropertyChange(IGradientColor.COLOR2, old, color2);
		colorPersistence.setColor2(color2.getRGB());
	}

}
