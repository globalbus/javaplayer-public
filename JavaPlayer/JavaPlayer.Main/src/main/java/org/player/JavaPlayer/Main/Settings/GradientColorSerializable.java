package org.player.JavaPlayer.Main.Settings;

import org.player.JavaPlayer.Settings.IGradientColorSerializable;

public class GradientColorSerializable implements IGradientColorSerializable {
	private Integer color1;
	private Integer color2;
	@Override
	public Integer getColor1() {
		return color1;
	}
	@Override
	public void setColor1(Integer color1) {
		this.color1 = color1;
	}
	@Override
	public Integer getColor2() {
		return color2;
	}
	@Override
	public void setColor2(Integer color2) {
		this.color2 = color2;
	}
}
