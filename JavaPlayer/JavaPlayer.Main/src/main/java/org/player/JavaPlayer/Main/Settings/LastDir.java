package org.player.JavaPlayer.Main.Settings;

import java.io.File;

import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Settings.ILastDir;
import org.player.JavaPlayer.Settings.ILastDirSerializable;

public class LastDir implements ILastDir {
	private File dir;
	private ILastDirSerializable serializable;
	@Override
	public File getDir(){
		return dir;
	}
	@Override
	public void setDir(File dir){
		this.dir=dir;
		serializable.setDir(dir.toString());
	}
	@Autowired
	public void setLastDirSerializable(ILastDirSerializable lastDir){
		serializable = lastDir;
		String dir = lastDir.getDir();
		if (dir != null) {
			File directory = new File(dir);
			if (directory != null && directory.exists()
					&& directory.isDirectory() && directory.canRead())
				this.dir=directory;
		}
	}

}
