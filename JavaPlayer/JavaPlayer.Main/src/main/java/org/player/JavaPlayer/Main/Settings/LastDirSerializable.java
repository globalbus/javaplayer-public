package org.player.JavaPlayer.Main.Settings;

import org.player.JavaPlayer.Settings.ILastDirSerializable;

public class LastDirSerializable implements ILastDirSerializable {
	private String dir;

	@Override
	public String getDir() {
			return dir;
	}

	@Override
	public void setDir(String dir) {
		this.dir = dir;
	}

}
