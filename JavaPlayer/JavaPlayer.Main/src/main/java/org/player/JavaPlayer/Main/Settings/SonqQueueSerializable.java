package org.player.JavaPlayer.Main.Settings;

import java.util.List;

import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Settings.ISongQueueSerializable;

public class SonqQueueSerializable implements ISongQueueSerializable {
	private List<ISongModel> songs;
	private ISongModel currentSong;
	private ICommandLineParser parser;
	private IJavaPlayerSupervisor supervisor;

	@Autowired
	public void setCommandLineParser(ICommandLineParser parser) {
		this.parser = parser;
	}

	@Autowired
	public void setSupervisor(IJavaPlayerSupervisor supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public void load() {
		if (songs != null) {
			supervisor.setPlaylist(parser.parse(songs));
			if (currentSong != null && songs.contains(currentSong))
				supervisor.setCurrentSong(currentSong);
		}

	}

	@Override
	public void setSongs(List<ISongModel> songsList) {
		this.songs = songsList;
	}

	@Override
	public void setCurrentSong(ISongModel currentSong) {
		this.currentSong = currentSong;

	}
	//for serialization
	public List<ISongModel> getSongs() {
		return songs;
	}
	//for serialization
	public ISongModel getCurrentSong() {
		return currentSong;
	}

}
