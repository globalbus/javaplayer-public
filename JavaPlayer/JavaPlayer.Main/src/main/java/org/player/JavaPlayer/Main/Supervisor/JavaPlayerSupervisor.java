package org.player.JavaPlayer.Main.Supervisor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.player.JavaPlayer.Contracts.Controllers.IPlayerController;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.State.PlayerState;

public class JavaPlayerSupervisor implements IJavaPlayerSupervisor, IActivate {
	private IPlayerController controller;
	@Override
	public IPlayerController getController() {
		return controller;
	}

	private ISongQueue songQueue;

	// publiczne funkcje
	@Override
	public void play() {
		controller.play();
	}

	@Override
	public void pause() {
		controller.pause();
	}

	@Override
	public void stop() {
		controller.stop();
	}

	@Override
	public void next() {
		ISongModel s = songQueue.getNext();
		if (s != null) {
			Logger.getLogger(Constants.APPNAME).log(Level.INFO,
					"Current song: " + s.getURI().getPath());
			setCurrentSong(s);
		}
	}

	@Override
	public void previous() {
		ISongModel s = songQueue.getPrevious();
		if (s != null) {
			Logger.getLogger(Constants.APPNAME).log(Level.INFO,
					"Current song: " + s.getURI().getPath());
			setCurrentSong(s);
		}
	}

	@Override
	public void setVolume(double vol) {
		controller.setVolume(vol);
	}

	@Override
	public double getVolume() {
		return controller.getVolume();
	}

	@Override
	public void setPosition(double percent) {
		controller.setPosition(percent);
	}

	@Override
	public void setPlaylist(ISongModel[] playlist) {
		for (ISongModel song : playlist) {
			songQueue.push(song);
		}
		next();
	}

	@Override
	public void setPlaylist(Collection<ISongModel> playlist) {
		setPlaylist(playlist.toArray(new ISongModel[0]));
	}

	@Override
	public ISongModel[] getPlaylist() {
		return songQueue.getAll();
	}

	@Override
	public void setCurrentSong(int index) {
		setCurrentSong(songQueue.getAt(index));
	}

	@Override
	public void setCurrentSong(ISongModel songModel) {
		if (songQueue.contains(songModel)) {
			songQueue.setCurrentSong(songModel);
			controller.setNewSong(songModel);
			play();
		}
	}

	@Override
	public ISongModel getCurrentSong() {
		return controller.getCurrentSong();
	}

	@Autowired
	public void setPlayerController(IPlayerController controller) {
		this.controller = controller;
	}

	@Autowired
	public void setSongQueue(ISongQueue songQueue) {
		this.songQueue = songQueue;
	}

	@Override
	public void activate() {
		controller.addPlayerStateListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				int newValue=(int) arg0.getNewValue();
				if(newValue==PlayerState.FINISHED){
					next();
				}
			}});
	}

	@Override
	public void remove(ISongModel newValue) {
		if(newValue==getCurrentSong())
			if(songQueue.getCount()>1)
				next();
			else
				stop();
		songQueue.remove(newValue);
	}

	@Override
	public void moveUp(ISongModel newValue) {
		songQueue.moveUp(newValue);
		
	}

	@Override
	public void moveDown(ISongModel newValue) {
		songQueue.moveDown(newValue);
	}

}
