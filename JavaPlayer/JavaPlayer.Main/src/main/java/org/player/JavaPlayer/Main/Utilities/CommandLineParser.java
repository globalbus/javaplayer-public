package org.player.JavaPlayer.Main.Utilities;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.player.JavaPlayer.Constants.ISupportedSchemas;
import org.player.JavaPlayer.Constants.SupportedFileTypes;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.ITagReader;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Core.SongModel;
import org.player.JavaPlayer.FileUtils.Commons;
import org.player.JavaPlayer.FileUtils.ID3TagReader;
import org.player.JavaPlayer.FileUtils.PLSEntry;

public class CommandLineParser implements ICommandLineParser {
	private static final String NUMBEROFENTRIES = "NumberOfEntries";
	private static final String FILE = "File";
	private static final String TITLE = "Title";
	private static final String LENGTH = "Length";
	private static final String VERSION = "Version";
	private static final String PLAYLISTHEADER = "[playlist]";
	private ITagReader tagReader;

	// konstruktor
	public CommandLineParser() {
		tagReader = new ID3TagReader();
	}

	/**
	 * Parsing command line input for opening files and playlists
	 * 
	 * @param args
	 */
	@Override
	public Collection<ISongModel> parse(String[] args) {
		Collection<ISongModel> models = new LinkedList<>();
		for (String i : args) {
			URI uri;
			try {
				uri = new URI(i);
			} catch (URISyntaxException e) {
				File f = new File(i);
				if (!f.canRead()) {
					Logger.getLogger(Constants.APPNAME).log(
							Level.SEVERE,
							String.format("File %s cannot be read",
									f.toString()));
					continue;
				}
				uri = f.toURI();
			}
			models.addAll(fileToModel(uri));
		}
		getTags(models);
		return models;
	}

	/**
	 * PLS strict format parser
	 * 
	 * @param PLS
	 *            file
	 * @return
	 */
	private Collection<PLSEntry> parsePls(URI uri) {
		try {
			Properties prop = new Properties();
			// thing to check - if hashcodes of integers is integer's
			// values, this map should be sorted. It's really? I don't
			// now...
			Map<Integer, PLSEntry> entries = new HashMap<>();
			prop.load(new InputStreamReader(uri.toURL().openStream()));
			Integer numberOfEntries = null;
			for (String propName : prop.stringPropertyNames()) {
				if (propName.equals(NUMBEROFENTRIES)) {
					try {
						numberOfEntries = Integer.parseInt(prop
								.getProperty(propName));
					} catch (NumberFormatException e) {
						Logger.getLogger(Constants.APPNAME).log(
								Level.SEVERE,
								String.format(
										"%s entry in %s is not numeric value",
										NUMBEROFENTRIES, uri.toString()));
					}
				} else if (propName.startsWith(FILE)) {
					try {
						int number = Integer.parseInt(propName.substring(FILE
								.length()));
						PLSEntry entry;
						if (entries.containsKey(number)) {
							entry = entries.get(number);
						} else {
							entry = new PLSEntry();
						}
						entry.setFile(new URL(prop.getProperty(propName))
								.toURI());
						entries.put(number, entry);
					} catch (NumberFormatException | URISyntaxException e) {
						Logger.getLogger(Constants.APPNAME).log(
								Level.SEVERE,
								String.format("%s entry in %s is invalid",
										FILE, uri.toString()));
					}
				} else if (propName.startsWith(TITLE)) {
					try {
						int number = Integer.parseInt(propName.substring(TITLE
								.length()));
						PLSEntry entry = null;
						if (entries.containsKey(number)) {
							entry = entries.get(number);
						} else {
							entry = new PLSEntry();
						}
						entry.setTitle(prop.getProperty(propName));
						entries.put(number, entry);
					} catch (NumberFormatException e) {
						Logger.getLogger(Constants.APPNAME).log(
								Level.SEVERE,
								String.format("%s entry in %s is invalid",
										TITLE, uri.toString()));
					}
				} else if (propName.startsWith(LENGTH)) {
					try {
						int number = Integer.parseInt(propName.substring(TITLE
								.length()));
						PLSEntry entry = null;
						if (entries.containsKey(number)) {
							entry = entries.get(number);
						} else {
							entry = new PLSEntry();
						}
						entry.setLength(Integer.parseInt(prop
								.getProperty(propName)));
						entries.put(number, entry);
					} catch (NumberFormatException e) {
						Logger.getLogger(Constants.APPNAME).log(
								Level.SEVERE,
								String.format("%s entry in %s is invalid",
										TITLE, uri.toString()));
					}
				} else if (propName.startsWith(VERSION)
						|| propName.startsWith(PLAYLISTHEADER))
					;// do nothing
				else {
					Logger.getLogger(Constants.APPNAME).log(
							Level.WARNING,
							String.format("unsupported entry %s in %s",
									propName, uri.toString()));
				}
			}
			if (numberOfEntries != null && numberOfEntries != entries.size()) {
				Logger.getLogger(Constants.APPNAME).log(
						Level.SEVERE,
						String.format("%s entry in %s is invalid",
								NUMBEROFENTRIES, uri.toString()));
			}
			for (Entry<Integer, PLSEntry> i : entries.entrySet()) {
				if (i.getValue().getFile() == null) {
					Logger.getLogger(Constants.APPNAME)
							.log(Level.SEVERE,
									String.format(
											"Entry %s in %s does not contain required file entry",
											i.getKey().toString(),
											uri.toString()));
					return null;
				}
			}
			return entries.values();
		} catch (IOException e) {
			Logger.getLogger(Constants.APPNAME).log(Level.SEVERE,
					String.format("Reading %s failed", FILE, uri.toString()));
		}
		return null;
	}

	@Override
	public Collection<ISongModel> parse(File[] files) {
		Collection<ISongModel> models = new LinkedList<>();
		for (File f : files) {
			if (f.canRead()) {
				models.addAll(fileToModel(f.toURI()));
			}
		}
		getTags(models);
		return models;
	}

	@Override
	public Collection<ISongModel> parse(Collection<ISongModel> models) {
		LinkedList<ISongModel> modelsTemp = new LinkedList<>(models);
		for (ISongModel i : modelsTemp)
			if (i.getURI() == null) {
				models.remove(i);
			} else if (ISupportedSchemas.FILE.equals(i.getURI().getScheme())) {
				try {
					File toTest = new File(i.getURI());
					if (!(toTest.exists() || toTest.canRead()))
						models.remove(i);
				} catch (Exception e) {
					models.remove(i);
				}
			}
		return models;
	}

	private Collection<ISongModel> fileToModel(URI uri) {
		Collection<ISongModel> models = new LinkedList<>();
		switch (Commons.getExtension(uri)) {
		case SupportedFileTypes.PLS:
			for (PLSEntry entry : parsePls(uri)) {
				ISongModel model = new SongModel();
				model.setURI(entry.getFile());
				models.add(model);
			}
			break;
		case SupportedFileTypes.MP3:
			ISongModel model = new SongModel();
			model.setURI(uri);
			models.add(model);
			break;
		default:
			Logger.getLogger(Constants.APPNAME).log(Level.WARNING,
					"Unsupported File in commandline arguments");
			break;
		}
		return models;
	}

	private void getTags(Collection<ISongModel> models) {
		for (ISongModel song : models) {
			tagReader.getTags(song);
		}
	}
}
