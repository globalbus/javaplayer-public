package org.player.JavaPlayer.Main;

import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Contracts.Main.IMainManager;
import org.player.JavaPlayer.IoC.PseudoContainer;

public class App {
	static {
		IMainManager manager = PseudoContainer.Instance
				.getSingleton(IMainManager.class);
		manager.startUp();
	}

	public static void main(String[] args) {
		ICommandLineParser parser = PseudoContainer.Instance
				.getSingleton(ICommandLineParser.class);
		ISongQueue queue = PseudoContainer.Instance
				.getSingleton(ISongQueue.class);
		for (ISongModel model : parser.parse(args)) {
			queue.push(model);
		}
	}
}
