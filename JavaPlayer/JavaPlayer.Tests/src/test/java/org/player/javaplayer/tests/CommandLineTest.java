package org.player.javaplayer.tests;

import static org.junit.Assert.*;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;

public class CommandLineTest {
	@Before
	public void prepare() throws InvocationTargetException, InterruptedException{
		App.main(new String[]{});
		SwingUtilities.invokeAndWait(Commons.Instance.getHide());
	}
	@After
	public void flush(){
		IFrameManager frame = PseudoContainer.Instance.getSingleton(IFrameManager.class);
		PseudoContainer.Instance.reload();
	}
	@Test
	public void SingleFileTest() throws URISyntaxException {
		String filename ="09-Kazik na Żywo-Plamy Na Słońcu.mp3";
		URL mp3TestFile = ClassLoader.getSystemResource(filename);
			App.main(new String[]{(new File(mp3TestFile.toURI())).toString()});
		ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
		assertEquals(1, queue.getCount());
		App.main(new String[]{(new File(mp3TestFile.toURI())).toString()});
		assertEquals(2, queue.getCount());
	}
	@Test
	public void LocalPLSTest() throws URISyntaxException{
		String localPath=ClassLoader.getSystemResource("listen.pls").toURI().toString();
		App.main(new String[]{localPath});
		ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
		assertEquals(1, queue.getCount());
	}
	@Test
	public void NetworkPLSTest(){
		String networkPath="http://153.19.65.164:8123/listen.pls";
		App.main(new String[]{networkPath});
		ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
		assertEquals(1, queue.getCount());
	}
}
