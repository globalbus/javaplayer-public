package org.player.javaplayer.tests;

import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;

public class Commons {

	public static Commons Instance;
	private Runnable hide;
	static {
		Instance = new Commons();
	}

	private Commons() {
		hide = new Runnable() {

			@Override
			public void run() {
				IFrameManager frame = PseudoContainer.Instance
						.getSingleton(IFrameManager.class);
				frame.setVisible(false);
			}

		};
	}

	public Runnable getHide() {
		return hide;
	}
}
