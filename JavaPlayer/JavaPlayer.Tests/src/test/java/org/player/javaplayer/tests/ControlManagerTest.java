/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.player.javaplayer.tests;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Views.IControlPanelManager;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;
import org.player.JavaPlayer.State.PlayerState;

/**
 * 
 * @author Mateusz Figat
 */
public class ControlManagerTest {
	@Before
	public void prepare() throws InvocationTargetException, InterruptedException {
		App.main(new String[] {});
		SwingUtilities.invokeAndWait(Commons.Instance.getHide());
	}

	@After
	public void flush() {
		IFrameManager frame = PseudoContainer.Instance
				.getSingleton(IFrameManager.class);
		PseudoContainer.Instance.reload();
	}

	@Test
	public void volumeTest() {
		IControlPanelManager panel = PseudoContainer.Instance
				.getSingleton(IControlPanelManager.class);
		panel.setVolume(20);
		Assert.assertEquals(1, panel.getVolume(), 0.1);
		panel.setVolume(-20);
		Assert.assertEquals(0, panel.getVolume(), 0.1);
		panel.setVolume(0.8);
		Assert.assertEquals(0.8, panel.getVolume(), 0.01);
		panel.setVolume(0.12);
		Assert.assertEquals(0.12, panel.getVolume(), 0.001);
	}

	@Test
	public void positionTest() {
		IControlPanelManager panel = PseudoContainer.Instance
				.getSingleton(IControlPanelManager.class);
		panel.setPosition(20);
		Assert.assertEquals(1, panel.getPosition(), 0.1);
		panel.setPosition(-20);
		Assert.assertEquals(0, panel.getPosition(), 0.1);
		panel.setPosition(0.8);
		Assert.assertEquals(0.8, panel.getPosition(), 0.01);
		panel.setPosition(0.12);
		Assert.assertEquals(0.12, panel.getPosition(), 0.001);
	}

	@Test
	public void stateTest() {
		IControlPanelManager panel = PseudoContainer.Instance
				.getSingleton(IControlPanelManager.class);
		panel.setState(PlayerState.SONG_NOT_SET);
		Assert.assertEquals(PlayerState.SONG_NOT_SET, panel.getState());
		panel.setState(PlayerState.SONG_SET);
		Assert.assertEquals(PlayerState.SONG_SET, panel.getState());
		panel.setState(PlayerState.PLAYING);
		Assert.assertEquals(PlayerState.PLAYING, panel.getState());
		panel.setState(PlayerState.PAUSED);
		Assert.assertEquals(PlayerState.PAUSED, panel.getState());
		panel.setState(PlayerState.STOPPED);
		Assert.assertEquals(PlayerState.STOPPED, panel.getState());
		panel.setState(PlayerState.FINISHED);
		Assert.assertEquals(PlayerState.FINISHED, panel.getState());
	}

}
