package org.player.javaplayer.tests;


import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Controllers.IPlayerController;
import org.player.JavaPlayer.Contracts.Core.IPlayerCore;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;
import org.player.JavaPlayer.State.PlayerState;

/**
 *
 * @author mgolebiewski
 */
public class JavaCoreTest {
    @Before
    public void prepare() throws InvocationTargetException, InterruptedException {
        App.main(new String[]{});
        SwingUtilities.invokeAndWait(Commons.Instance.getHide());
    }
    
    @After
    public void flush() {
        IFrameManager frame = PseudoContainer.Instance.getSingleton(IFrameManager.class);
        PseudoContainer.Instance.reload();
    }
    @Test
    public synchronized void getState() throws InterruptedException, URISyntaxException {
    	IPlayerCore player = PseudoContainer.Instance.getSingleton(IPlayerCore.class);
        Assert.assertEquals(player.getState(), PlayerState.SONG_NOT_SET);

        ICommandLineParser cmd = PseudoContainer.Instance.getSingleton(ICommandLineParser.class);
        IPlayerController controller = PseudoContainer.Instance.getSingleton(IPlayerController.class);
        //String []args = {"http://153.19.65.164:8123/listen.pls"};
		String filename ="09-Kazik na Żywo-Plamy Na Słońcu.mp3";
		URL mp3TestFile = ClassLoader.getSystemResource(filename);
        Collection<ISongModel> songList = cmd.parse(new String[]{(new File(mp3TestFile.toURI())).toString()});
        
        if ( songList.isEmpty() ) {
            Assert.fail("No songs in songList or no internet connection");
            return;
        }
        
        controller.setNewSong(songList.iterator().next());
        this.wait(100);//we need to wait
        Assert.assertEquals(player.getState(), PlayerState.SONG_SET);
        
        player.play();
        this.wait(100);//we need to wait
        Assert.assertEquals(player.getState(), PlayerState.PLAYING);
        
        player.pause();
        this.wait(100);//we need to wait
        Assert.assertEquals(player.getState(), PlayerState.PAUSED);
        
        player.play();
        this.wait(100);//we need to wait
        Assert.assertEquals(player.getState(), PlayerState.PLAYING);
        
        player.stop();
        this.wait(100);//we need to wait
        Assert.assertEquals(player.getState(), PlayerState.STOPPED);
    }

    @Test
    public void getSong() {
    	IPlayerCore player = PseudoContainer.Instance.getSingleton(IPlayerCore.class);
        Assert.assertEquals(player.getSong(), null);
    }

}
