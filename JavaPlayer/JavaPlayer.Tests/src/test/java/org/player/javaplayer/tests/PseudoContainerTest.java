package org.player.javaplayer.tests;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Common.IControlPanelConfig;
import org.player.JavaPlayer.Contracts.Common.IMenuBarConfig;
import org.player.JavaPlayer.Contracts.Common.IQueuePanelConfig;
import org.player.JavaPlayer.Contracts.Controllers.IPlayerController;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.IPlayerCore;
import org.player.JavaPlayer.Contracts.IoC.IContainer;
import org.player.JavaPlayer.Contracts.Main.ICommandLineParser;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Contracts.Main.IMainManager;
import org.player.JavaPlayer.Contracts.Main.IResource;
import org.player.JavaPlayer.Contracts.Views.IControlPanelManager;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.Contracts.Views.IMenuBarManager;
import org.player.JavaPlayer.Contracts.Views.IQueuePanelManager;
import org.player.JavaPlayer.Contracts.Views.ITrayView;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;
import org.player.JavaPlayer.Settings.ILastDirSerializable;

/**
 *
 * @author thegrymek
 */
public class PseudoContainerTest {
    
    @Before
    public void prepare() throws InvocationTargetException, InterruptedException {
        App.main(new String[]{});
        SwingUtilities.invokeAndWait(Commons.Instance.getHide());
    }
    
    @After
    public void flush() {
        IFrameManager frame = PseudoContainer.Instance.getSingleton(IFrameManager.class);
        PseudoContainer.Instance.reload();
    }
    
    @Test
    public void checkRegistredObjects()
    {
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IContainer.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IFrameManager.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IPlayerCore.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IPlayerController.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(ISongQueue.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IJavaPlayerSupervisor.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IQueuePanelManager.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IControlPanelManager.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IMenuBarManager.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(ICommandLineParser.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IResource.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(ITrayView.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IMainManager.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IControlPanelConfig.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IMenuBarConfig.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(IQueuePanelConfig.class), null);
        Assert.assertNotEquals(PseudoContainer.Instance.getSingleton(ILastDirSerializable.class), null);
    }
    
    @Test
    public void getSingleton()
    {
        PseudoContainer.Instance.putSingleton(new Ab());
        
        IA a = PseudoContainer.Instance.getSingleton(IA.class);
        IB b = PseudoContainer.Instance.getSingleton(IB.class);
        Assert.assertEquals(a, b);
        
        PseudoContainer.Instance.putSingleton(new C());
        IA ca = PseudoContainer.Instance.getSingleton(IA.class);
        IC cc = PseudoContainer.Instance.getSingleton(IC.class);
        Assert.assertNotEquals(ca, cc);
    }
    
    
    ///////////////////////////////////////////////////////////////////
    // used in getSingleton() test
    private class Ab implements IA, IB {}
    private interface IA {}
    private interface IB {}
    private interface IC {}
    private class C extends Ab implements IC {}
    
}
