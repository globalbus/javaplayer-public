package org.player.javaplayer.tests;

import java.awt.Image;
import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Main.IResource;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;

/**
 *
 * @author thegrymek
 */
public class ResourceTest {

    @Before
    public void prepare() throws InvocationTargetException, InterruptedException {
        App.main(new String[]{});
        SwingUtilities.invokeAndWait(Commons.Instance.getHide());
    }

    @After
    public void flush() {
        IFrameManager frame = PseudoContainer.Instance.getSingleton(IFrameManager.class);
        PseudoContainer.Instance.reload();
    }

    @Test
    public void getIcon() {
        IResource res = PseudoContainer.Instance.getSingleton(IResource.class);
        Image img = res.getIcon();
        Assert.assertNotEquals(img, null);
    }
    
    
}
