package org.player.javaplayer.tests;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.SwingUtilities;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.IoC.PseudoContainer;
import org.player.JavaPlayer.Main.App;

/**
 *
 * @author mgolebiewski
 */
public class SongQueueTest {
    @Before
    public void prepare() throws InvocationTargetException, InterruptedException {
        App.main(new String[]{});
        SwingUtilities.invokeAndWait(Commons.Instance.getHide());
    }
    
    @After
    public void flush() {
        IFrameManager frame = PseudoContainer.Instance.getSingleton(IFrameManager.class);
        PseudoContainer.Instance.reload();
    }

    @Test
    public void emptyList() {
    	ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
        ArrayList<ISongModel> emptyList = new ArrayList<>();
        Assert.assertArrayEquals(queue.getAll(), emptyList.toArray());
        getNextSong();
        getPreviousSong();
    }

    public void getNextSong() {
    	ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
        Assert.assertEquals(queue.getNext(), null);
    }

    @Test
    public void getPreviousSong() {
    	ISongQueue queue = PseudoContainer.Instance.getSingleton(ISongQueue.class);
        Assert.assertEquals(queue.getPrevious(), null);
    }

}
