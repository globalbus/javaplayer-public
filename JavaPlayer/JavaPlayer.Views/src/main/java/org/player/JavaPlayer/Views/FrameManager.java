package org.player.JavaPlayer.Views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.player.JavaPlayer.Contracts.IoC.IContainer;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.IoC.IDisposable;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Main.IResource;
import org.player.JavaPlayer.Contracts.Views.IControlPanelManager;
import org.player.JavaPlayer.Contracts.Views.IFrameManager;
import org.player.JavaPlayer.Contracts.Views.IMenuBarManager;
import org.player.JavaPlayer.Contracts.Views.IQueuePanelManager;
import org.player.JavaPlayer.Settings.IGradientColor;

public class FrameManager implements IFrameManager, IDisposable {

	private JFrame frame;
	IContainer container;
	GradientPanel gradientPanel;

	public FrameManager() {
		init();
	}

	@Autowired
	public void setResource(IResource resources) {
		frame.setIconImage(resources.getIcon());
	}

	@Autowired
	public void setControlPanel(IControlPanelManager controlPanelManager) {
		frame.add(controlPanelManager.getPanel(), BorderLayout.NORTH);
	}

	@Autowired
	public void setQueuePanel(IQueuePanelManager queuePanelManager) {
		frame.add(queuePanelManager.getPanel(), BorderLayout.CENTER);
	}

	@Autowired
	public void setMenuBar(IMenuBarManager menuBarManager) {
		frame.setJMenuBar(menuBarManager.getMenuBar());
	}
	@Autowired
	public void setGradientColor(IGradientColor gradientColor){
		PropertyChangeListener gradientListener = new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent arg0) {
				if(arg0.getPropertyName()==IGradientColor.COLOR1){
					gradientPanel.setColor1((Color) arg0.getNewValue());
				}
				else if(arg0.getPropertyName()==IGradientColor.COLOR2){
					gradientPanel.setColor2((Color) arg0.getNewValue());
				}
				gradientPanel.repaint();
			}
			
		};
		gradientColor.addPropertyChangeListener(gradientListener);
		gradientPanel.setColor1(gradientColor.getColor1());
		gradientPanel.setColor2(gradientColor.getColor2());
	}
	
	@Override
	public void setVisible(boolean visible) {
		frame.pack();
		frame.setVisible(visible);
	}

	@Autowired
	public void setContainer(IContainer container) {
		this.container = container;
	}

	private void init() {
		frame = new JFrame(Constants.APPNAME);
		gradientPanel = new GradientPanel();
		frame.setContentPane(gradientPanel);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				container.dispose();
			}

		});
		frame.getContentPane().setBackground(Color.BLACK);

	}
	@Override
	public void deactivate() {
		frame.dispose();
	}
}
class GradientPanel extends JPanel{
	private Color color1=Color.WHITE;
	private Color color2=Color.WHITE;
	public void setColor1(Color color1) {
		this.color1 = color1;
	}

	public void setColor2(Color color2) {
		this.color2 = color2;
	}

	private static final long serialVersionUID = 1L;

	// blok inicjalizacyjny
	{
		setOpaque(false);
	}

	@Override
	protected void paintComponent(Graphics grphcs) {
		Graphics2D g2d = (Graphics2D) grphcs;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		GradientPaint gp = new GradientPaint(new Point(0, 0), color1,
				new Point(0, getHeight()), color2);

		g2d.setPaint(gp);
		g2d.fillRect(0, 0, getWidth(), getHeight());

		super.paintComponent(grphcs);
	}
	
}
