package org.player.JavaPlayer.Views.MenuBar;

import java.awt.event.ActionListener;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Common.IMenuBarConfig;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.MenuBarConfigID;
import org.player.JavaPlayer.Contracts.Views.IMenuBarManager;

public class MenuBarManager implements IMenuBarManager, IActivate {
	private JMenuBar menuBar;
	private JMenu fileMenu;
	private JMenuItem fileMenuOpenItem;
	private JMenuItem fileMenuExitItem;
	private IConfiguration configuration;
	private JMenuItem fileMenuOpenURLItem;
	private JMenuItem settingsMenu;

	@Override
	public void activate() {
		configuration.init();
		setConfiguration(configuration);
	}

	// konstruktor
	public MenuBarManager() {
		init();
	}

	@Override
	public JMenuBar getMenuBar() {
		return menuBar;
	}

	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();
		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}

	@Autowired
	public void setConfiguration(IMenuBarConfig configuration) {
		this.configuration = configuration;
	}

	// publiczne funkcje
	public void addMenuOpenItemListener(ActionListener listener) {
		fileMenuOpenItem.addActionListener(listener);
	}

	public void addMenuExitItemListener(ActionListener listener) {
		fileMenuExitItem.addActionListener(listener);
	}

	private void addMenuOpenURLItemListener(ActionListener listener) {
		fileMenuOpenURLItem.addActionListener(listener);

	}

	public void removeMenuOpenItemListener(ActionListener listener) {
		fileMenuOpenItem.removeActionListener(listener);
	}

	public void removeExitOpenItemListener(ActionListener listener) {
		fileMenuExitItem.removeActionListener(listener);
	}
	private void addMenuSettingsOpenListener(ActionListener config) {
		settingsMenu.addActionListener(config);
		
	}
	// prywatne funkcje
	private void init() {
		menuBar = new JMenuBar();

		// tworzenie menu "Plik"
		fileMenu = new JMenu("Plik");
		settingsMenu = new JMenuItem("Ustawienia");

		// tworzenie elementów menu "Plik"
		fileMenuOpenItem = new JMenuItem("Otwórz");
		fileMenuOpenURLItem = new JMenuItem("Otwórz adres");
		fileMenuExitItem = new JMenuItem("Wyjdź");

		// dodawanie elementów do menu "Plik"
		fileMenu.add(fileMenuOpenItem);
		fileMenu.add(fileMenuOpenURLItem);
		fileMenu.add(fileMenuExitItem);

		// dodanie menu "Plik" do paska menu
		menuBar.add(fileMenu);
		menuBar.add(settingsMenu);
	}

	private void _makeSetting(int id, Object config){
        switch(id){
            case MenuBarConfigID.MENU_OPEN_ITEM_LISTENER:
                addMenuOpenItemListener((ActionListener)config);
                break;
            case MenuBarConfigID.MENU_OPEN_URL_ITEM_LISTENER:
                addMenuOpenURLItemListener((ActionListener)config);
                break;
                
            case MenuBarConfigID.MENU_EXIT_ITEM_LISTENER:
                addMenuExitItemListener((ActionListener)config);
                break;
            case MenuBarConfigID.MENU_SETTINGS_OPEN_LISTENER:
                addMenuSettingsOpenListener((ActionListener)config);
                break;
                
                
            default:
            	Logger.getLogger(Constants.APPNAME).log(Level.WARNING,"Pominięto nieznaną konfigurację MenuBar'a"+
                        "\n\tID: " + id +
                        "\n\tConfig: " + config.getClass());
        }
    }



}
