package org.player.JavaPlayer.Views.Panels.ControlPanel;

import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Common.IControlPanelConfig;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.ControlPanelConfigID;
import org.player.JavaPlayer.Contracts.Views.IControlPanelManager;
import org.player.JavaPlayer.State.PlayerState;

public class ControlPanelManager implements IControlPanelManager, IActivate {

	private ControlPanel panel;
	private JButton playButton;
	private JButton pauseButton;
	private JButton stopButton;
	private JButton nextButton;
	private JButton prevButton;

	private JSlider volumeSlider;
	private JSlider positionSlider;

	double currentVolume;
	double currentPosition;

	private ChangeListener volumeSliderChangeListener;
	private MouseAdapter positionSliderMouseAdapter;

	PropertyChangeSupport volumeChangeNotifier;
	PropertyChangeSupport positionChangeNotifier;

	private int currentState;
	boolean positionSliderInUse;
	private IConfiguration configuration;
	@Override
	public void activate() {
		setConfiguration(configuration);
	}
	// konstruktor
	public ControlPanelManager() {
		_init();
	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

	@Override
	public double getVolume() {
		return currentVolume;
	}

	@Override
	public void setVolume(double vol) {
		currentVolume = _validateDouble(vol);
		_updateSlider(Sliders.VOLUME);
	}

	@Override
	public double getPosition() {
		return currentPosition;
	}

	@Override
	public void setPosition(double position) {
		if (positionSliderInUse) {
			return;
		}

		currentPosition = _validateDouble(position);
		_updateSlider(Sliders.POSITION);
	}

	@Override
	public void setState(int state) {
		currentState = state;
		_updateState();
	}

	@Override
	public int getState() {
		return currentState;
	}

	@Override
	public boolean isPositionSliderInUse() {
		return positionSliderInUse;
	}

	// add listener
	private void addPlayButtonActionListener(ActionListener listener) {
		playButton.addActionListener(listener);
	}

	private void addPauseButtonActionListener(ActionListener listener) {
		pauseButton.addActionListener(listener);
	}

	private void addStopButtonActionListener(ActionListener listener) {
		stopButton.addActionListener(listener);
	}

	private void addNextButtonActionListener(ActionListener listener) {
		nextButton.addActionListener(listener);
	}

	private void addPrevButtonActionListener(ActionListener listener) {
		prevButton.addActionListener(listener);
	}

	private void addVolumeChangeListener(PropertyChangeListener listener) {
		volumeChangeNotifier.addPropertyChangeListener(listener);
	}

	private void addPositionChangeListener(PropertyChangeListener listener) {
		positionChangeNotifier.addPropertyChangeListener(listener);
	}

	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();
		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}

	@Autowired
	public void setConfiguration(IControlPanelConfig configuration) {
		this.configuration=configuration;
	}

	// prywatne funkcje
	private void _init() {
		// panel = new JPanel(new WrapLayout());
		panel = new ControlPanel();
		volumeChangeNotifier = new PropertyChangeSupport(this);
		positionChangeNotifier = new PropertyChangeSupport(this);

		// Buttons
		playButton = panel.getPlayButton();
		pauseButton = panel.getPauseButton();
		stopButton = panel.getStopButton();
		nextButton = panel.getNextButton();
		prevButton = panel.getPrevButton();

		// Sliders
		volumeSlider = panel.getVolumeSlider();
		positionSlider = panel.getPositionSlider();

		_add_SlidersChangeListeners();

		// init values
		currentVolume = _calcProperSliderValue(Sliders.VOLUME);
		currentPosition = _calcProperSliderValue(Sliders.POSITION);
		positionSliderInUse = false;
	}

	private void _add_SlidersChangeListeners() {
		_create_SlidersMouseAdapter();

		volumeSlider.addChangeListener(volumeSliderChangeListener);
		positionSlider.addMouseListener(positionSliderMouseAdapter);
	}

	private void _create_SlidersMouseAdapter() {
		_create_VolumeSliderChangeListener();
		_create_PositionSliderMouseAdapter();
	}

	private void _create_VolumeSliderChangeListener() {
		volumeSliderChangeListener = new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				double oldVolume = currentVolume;
				currentVolume = _calcSliderPercent(Sliders.VOLUME);

				volumeChangeNotifier.firePropertyChange(null, oldVolume,
						currentVolume);
			}
		};
	}

	private void _create_PositionSliderMouseAdapter() {
		positionSliderMouseAdapter = new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				positionSliderInUse = true; // zablokowanie slidera
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				double oldPosition = currentPosition;
				currentPosition = _calcSliderPercent(Sliders.POSITION);

				positionChangeNotifier.firePropertyChange(null, oldPosition,
						currentPosition);
				positionSliderInUse = false;
			}
		};
	}

	// helpers
	private double _validateDouble(double value) {
		double validated = value;

		if (validated < 0) {
			validated = 0;
		} else if (validated > 1) {
			validated = 1;
		}

		return validated;
	}

	private int _calcProperSliderValue(Sliders slider) {
		double percent = 0.005; // w celu lepszego zaokrąglenia +0.005;
		int maxValue = -1;

		switch (slider) {
		case VOLUME:
			percent += currentVolume;
			maxValue = volumeSlider.getMaximum();
			break;

		case POSITION:
			percent += currentPosition;
			maxValue = positionSlider.getMaximum();
			break;
		default:
			break;
		}

		return (int) (percent * maxValue);
	}

	double _calcSliderPercent(Sliders slider) {
		int value = -1;
		int maxValue = -1;
		switch (slider) {
		case VOLUME:
			value = volumeSlider.getValue();
			maxValue = volumeSlider.getMaximum();
			break;
		case POSITION:
			value = positionSlider.getValue();
			maxValue = positionSlider.getMaximum();
			break;
		default:
			break;
		}

		return ((double) value) / maxValue;
	}

	private void _updateSlider(Sliders slider) {
		int sliderValue = _calcProperSliderValue(slider);
		switch (slider) {
		case VOLUME:
			volumeSlider.setValue(sliderValue);
			break;

		case POSITION:
			positionSlider.setValue(sliderValue);
			break;
		default:
			break;
		}
	}

	private void _makeSetting(int id, Object config) {
		switch (id) {
		case ControlPanelConfigID.PLAY_BUTTON_ACTION_LISTENER:
			addPlayButtonActionListener((ActionListener) config);
			break;

		case ControlPanelConfigID.PAUSE_BUTTON_ACTION_LISTENER:
			addPauseButtonActionListener((ActionListener) config);
			break;

		case ControlPanelConfigID.STOP_BUTTON_ACTION_LISTENER:
			addStopButtonActionListener((ActionListener) config);
			break;

		case ControlPanelConfigID.NEXT_BUTTON_ACTION_LISTENER:
			addNextButtonActionListener((ActionListener) config);
			break;

		case ControlPanelConfigID.PREV_BUTTON_ACTION_LISTENER:
			addPrevButtonActionListener((ActionListener) config);
			break;

		case ControlPanelConfigID.VOLUME_SLIDER_CHANGE_LISTENER:
			addVolumeChangeListener((PropertyChangeListener) config);
			break;

		case ControlPanelConfigID.POSITION_SLIDER_CHANGE_LISTENER:
			addPositionChangeListener((PropertyChangeListener) config);
			break;

		case ControlPanelConfigID.VOLUME_VALUE:
			setVolume((double) config);
			break;

		case ControlPanelConfigID.POSITION_VALUE:
			setPosition((double) config);
			break;

		case ControlPanelConfigID.CURRENT_STATE:
			setState((int) config);
			break;

		default:
			Logger.getLogger(Constants.APPNAME).log(Level.WARNING,"Pominięto nie znaną konfigurację ControlPanelu"
					+ "\n\tID: " + id + "\n\tConfig: " + config.getClass());
		}
	}

	// buttons enabled
	private void _updateState() {
		switch (currentState) {
		case PlayerState.SONG_NOT_SET:
			_allDisabled();
			break;

		case PlayerState.SONG_SET:
			_onReady();
			break;

		case PlayerState.PLAYING:
			_onPlaying();
			break;

		case PlayerState.PAUSED:
			_onPause();
			break;

		case PlayerState.STOPPED:
			_onStopped();
			break;

		case PlayerState.FINISHED:
			_onReady();
			break;
		default:
			break;
		}
	}

	private void _allDisabled() {
		playButton.setEnabled(false);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);

		nextButton.setEnabled(false);
		prevButton.setEnabled(false);

		positionSlider.setEnabled(false);
	}

	private void _onReady() {
		playButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);

		nextButton.setEnabled(false);
		prevButton.setEnabled(false);

		positionSlider.setEnabled(true);
	}

	private void _onPlaying() {
		playButton.setEnabled(false);
		pauseButton.setEnabled(true);
		stopButton.setEnabled(true);

		nextButton.setEnabled(true);
		prevButton.setEnabled(true);

		positionSlider.setEnabled(true);
	}

	private void _onPause() {
		playButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(true);

		nextButton.setEnabled(true);
		prevButton.setEnabled(true);

		positionSlider.setEnabled(true);
	}

	private void _onStopped() {
		playButton.setEnabled(true);
		pauseButton.setEnabled(false);
		stopButton.setEnabled(false);

		nextButton.setEnabled(true);
		prevButton.setEnabled(true);

		positionSlider.setEnabled(true);
	}
}
