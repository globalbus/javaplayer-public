package org.player.JavaPlayer.Views.Panels.QueuePanel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Common.IQueuePanelConfig;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.QueuePanelConfigID;
import org.player.JavaPlayer.Contracts.Views.IQueuePanelManager;

public class QueuePanelManager implements IQueuePanelManager, IActivate {
	private QueuePanel panel;
	JTable queue;
	private SongTableCellRenderer cellRenderer;

	SongTableModel songTableModel;
	private int currentSong;

	private MouseAdapter songSelectionMouseAdapter;
	PropertyChangeSupport newSongSelected;
	private IConfiguration configuration;
	JPopupMenu rightClickMenu;

	@Override
	public void activate() {
		setConfiguration(configuration);
	}

	// konstruktor
	public QueuePanelManager() {
		_init();
	}

	@Autowired
	public void setQueueModel(ISongQueue songQueue) {
		songTableModel = new SongTableModel(songQueue);
		queue.setModel(songTableModel);
	}

	@Override
	public void setCurrentSong(ISongModel songModel) {
		// sprawdzenie poprawności indeksu
		int index = songTableModel.getIndexOf(songModel);
		if (index < 0) {
			return;
		}

		currentSong = index;
		cellRenderer.setCurrentSong(currentSong);
		queue.repaint();
	}

	ISongModel getCurrentSong() {
		return songTableModel.getSongModel(currentSong);
	}

	ISongModel getSelectedSong() {
		int index = queue.getSelectedRow();
		return songTableModel.getSongModel(index);
	}

	private void addSongSelectionListener(PropertyChangeListener listener) {
		newSongSelected.addPropertyChangeListener(listener);
	}

	@Override
	public JPanel getPanel() {
		return panel;
	}

	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();
		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}

	@Autowired
	public void setConfiguration(IQueuePanelConfig configuration) {
		this.configuration = configuration;
	}

	// prywatne funkcje
	private void _init() {
		panel = new QueuePanel();
		queue = panel.getTable();
		rightClickMenu = new JPopupMenu();
		JMenuItem removeItem = new JMenuItem("Usuń");
		removeItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				newSongSelected.firePropertyChange(IQueuePanelConfig.REMOVE, null,
						getSelectedSong());
			}});
		rightClickMenu.add(removeItem);
		JMenuItem moveUpItem = new JMenuItem("w górę");
		moveUpItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				newSongSelected.firePropertyChange(IQueuePanelConfig.MOVEUP, null,
						getSelectedSong());
			}});
		rightClickMenu.add(moveUpItem);
		JMenuItem moveDownItem = new JMenuItem("w dół");
		moveDownItem.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				newSongSelected.firePropertyChange(IQueuePanelConfig.MOVEDOWN, null,
						getSelectedSong());
			}});
		rightClickMenu.add(moveDownItem);
		
		cellRenderer = new SongTableCellRenderer();
		newSongSelected = new PropertyChangeSupport(this);

		queue.setDefaultRenderer(Object.class, cellRenderer);

		_create_SongSelectionMouseAdapter();
		queue.addMouseListener(songSelectionMouseAdapter);
	}

	private void _makeSetting(int id, Object config) {
		switch (id) {
		case QueuePanelConfigID.SONG_SELECTION_LISTENER:
			addSongSelectionListener((PropertyChangeListener) config);
			break;

		case QueuePanelConfigID.SET_CURRENT_SONG:
			setCurrentSong((ISongModel) config);
			break;

		default:
			Logger.getLogger(Constants.APPNAME).log(
					Level.WARNING,
					"Pominięto nie znaną konfigurację QueuePanelu" + "\n\tID: "
							+ id + "\n\tConfig: " + config.getClass());
		}
	}

	private void _create_SongSelectionMouseAdapter() {
		songSelectionMouseAdapter = new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent evt) {
				if (evt.getClickCount() == 2) {
					newSongSelected.firePropertyChange(IQueuePanelConfig.DOUBLECLICK, getCurrentSong(),
							getSelectedSong());
				}
				if (evt.getButton()==MouseEvent.BUTTON3) {
					int row = queue.rowAtPoint(evt.getPoint());
					queue.changeSelection(row, 0, false, false);
					rightClickMenu.show(queue, evt.getX(), evt.getY());
				}
			}
		};
	}
}
