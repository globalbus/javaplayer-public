package org.player.JavaPlayer.Views.Panels.QueuePanel;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

class SongTableCellRenderer extends DefaultTableCellRenderer{
	private static final long serialVersionUID = 1L;
	private int currentSong;
    
    public void setCurrentSong(int currentSong) {
        this.currentSong = currentSong;
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component cell = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        
        if(currentSong == row) {
            cell.setForeground(Color.RED);
        } else {
            cell.setForeground(Color.BLACK);
        }
        return cell;
    }
}
