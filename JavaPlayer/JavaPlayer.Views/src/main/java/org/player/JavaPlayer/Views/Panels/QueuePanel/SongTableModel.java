package org.player.JavaPlayer.Views.Panels.QueuePanel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.table.AbstractTableModel;
import org.player.JavaPlayer.Contracts.Controllers.ISongQueue;
import org.player.JavaPlayer.Contracts.Core.ISongModel;

class SongTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private ISongQueue queue;
	private boolean editable;

	private PropertyChangeListener songQueuePushListener;

	public SongTableModel(ISongQueue queue) {
		this.queue = queue;
		_init();
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public int getIndexOf(ISongModel songModel) {
		return queue.getIndexOf(songModel);
	}

	public ISongModel getSongModel(int index) {
		return queue.getAt(index);
	}

	@Override
	public int getColumnCount() {
		return 1;
	}

	@Override
	public int getRowCount() {
		return queue.getCount();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		ISongModel object = queue.getAt(rowIndex);
		return object.getURI().getPath();
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return editable;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch (columnIndex) {
		case 0:
			return "Playlist";
		default:
			break;
		}
		return null;
	}

	// prywatne funkcje
	private void _init() {
		editable = false;

		_addListeners();
	}

	private void _addListeners() {
		_create_SongQueuePushListener();

		queue.addPropertyChangeListener(ISongQueue.PUSH, songQueuePushListener);
	}

	private void _create_SongQueuePushListener() {
		songQueuePushListener = new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				fireTableDataChanged();
			}
		};
	}
}