package org.player.JavaPlayer.Views.Settings;

import java.awt.Component;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Common.IGradientPanelConfig;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Views.IGradientSettings;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.GradientPanelConfigID;
import org.player.JavaPlayer.Settings.IGradientColor;

public class GradientSettingsPanel implements IGradientSettings, IActivate{
	private JPanel contentPane;
	private JButton color2Button;
	private JButton color1Button;
	private IConfiguration configuration;
	private IGradientColor color;
	public GradientSettingsPanel(){
		init();
		
	}
	@Autowired
	public void setGradientColor(IGradientColor color) {
		this.color=color;
	}
	@Autowired
	public void setConfiguration(IGradientPanelConfig configuration) {
		this.configuration = configuration;
	}
	private void init(){
		contentPane= new JPanel();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		JLabel color1Label = new JLabel("Kolor pierwszy");
		color1Button = new JButton();
		color1Button.setActionCommand(IGradientColor.COLOR1);
		contentPane.add(color1Label);
		contentPane.add(color1Button);
		JLabel color2Label = new JLabel("Kolor drugi");
		color2Button = new JButton();
		color2Button.setActionCommand(IGradientColor.COLOR2);
		contentPane.add(color2Label);
		contentPane.add(color2Button);
	}
	@Override
	public Component getComponent(){
		return contentPane;
	}
	@Override
	public String getName(){
		return "Kolory";
	}
	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();
		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}
	private void _makeSetting(int id, Object config){
        switch(id){
            case GradientPanelConfigID.CHANGE_COLOR_BUTTON_LISTENER:
                addChangeColorButtonListener((ActionListener)config);
                break;
                
            default:
            	Logger.getLogger(Constants.APPNAME).log(Level.WARNING,"Pominięto nieznaną konfigurację GradientSettingPanel'a"+
                        "\n\tID: " + id +
                        "\n\tConfig: " + config.getClass());
        }
    }
	private void addChangeColorButtonListener(ActionListener config) {
		color1Button.addActionListener(config);
		color2Button.addActionListener(config);
	}

	@Override
	public void activate() {
		setConfiguration(configuration);
		reload();
	}
	@Override
	public void reload() {
		color1Button.setBackground(color.getColor1());
		color2Button.setBackground(color.getColor2());
	}
	@Override
	public void save() {
		color.setColor1(color1Button.getBackground());
		color.setColor2(color2Button.getBackground());
	}
}
