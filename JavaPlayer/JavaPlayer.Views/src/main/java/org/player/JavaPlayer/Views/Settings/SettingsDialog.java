package org.player.JavaPlayer.Views.Settings;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.WindowConstants;

import org.player.JavaPlayer.Contracts.Common.IConfiguration;
import org.player.JavaPlayer.Contracts.Common.ISettingsDialogConfig;
import org.player.JavaPlayer.Contracts.Common.ISettingsPanel;
import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.Constants;
import org.player.JavaPlayer.Contracts.Main.IResource;
import org.player.JavaPlayer.Contracts.Views.IGradientSettings;
import org.player.JavaPlayer.Contracts.Views.ISettingsDialog;
import org.player.JavaPlayer.Contracts.Views.ConfigsID.SettingsDialogConfigID;

public class SettingsDialog implements ISettingsDialog, IActivate {
	private JDialog dialog;
	private JTabbedPane tabs;
	private JButton buttonOk;
	private JButton buttonCancel;
	private IConfiguration configuration;
	private List<ISettingsPanel> panels;
	@Autowired
	public void setConfiguration(ISettingsDialogConfig configuration) {
		this.configuration = configuration;
	}
	@Autowired
	public void setResource(IResource resources) {
		dialog.setIconImage(resources.getIcon());
	}
	@Autowired
	public void setGradientDialog(IGradientSettings gradient){
		tabs.add(gradient.getName(), gradient.getComponent());
		panels.add(gradient);
	}
	public SettingsDialog(){
		init();
	}
	private void init(){
		panels = new LinkedList<>();
		dialog= new JDialog();
		dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
		dialog.setBounds(100,100,400,400);
		dialog.setLayout(new BorderLayout());
		tabs = new JTabbedPane();
		dialog.add(tabs, BorderLayout.CENTER);
		JPanel buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
		buttonOk = new JButton("OK");
		buttonCancel = new JButton("Anuluj");
		buttons.add(buttonOk);
		buttons.add(buttonCancel);
		dialog.add(buttons, BorderLayout.SOUTH);
	}
	@Override
	public void setConfiguration(IConfiguration configuration) {
		Map<Integer, Object> config = configuration.getConfig();
		for (Integer id : config.keySet()) {
			_makeSetting(id, config.get(id));
		}
	}
	private void _makeSetting(int id, Object config){
        switch(id){
            case SettingsDialogConfigID.OK_LISTENER:
                addOkButtonListener((ActionListener)config);
                break;
                
            case SettingsDialogConfigID.CANCEL_LISTENER:
                addCancelButtonListener((ActionListener)config);
                break;
                
            default:
            	Logger.getLogger(Constants.APPNAME).log(Level.WARNING,"Pominięto nieznaną konfigurację SettingsDialog'a"+
                        "\n\tID: " + id +
                        "\n\tConfig: " + config.getClass());
        }
    }
	private void addCancelButtonListener(ActionListener config) {
		buttonCancel.addActionListener(config);
	}
	private void addOkButtonListener(ActionListener config) {
		buttonOk.addActionListener(config);
	}
	@Override
	public void setVisible(boolean visible){
		dialog.setVisible(visible);
		if(visible){
			dialog.toFront();
		}
	}
	@Override
	public void activate() {
		setConfiguration(configuration);
		
	}
	@Override
	public void reload() {
		for(ISettingsPanel i:panels)
			i.reload();
	}
	@Override
	public void saveSettings() {
		for(ISettingsPanel i:panels)
			i.save();
	}
}
