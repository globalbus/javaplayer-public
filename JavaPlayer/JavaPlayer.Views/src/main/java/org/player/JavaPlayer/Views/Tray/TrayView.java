package org.player.JavaPlayer.Views.Tray;

import java.awt.AWTException;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.player.JavaPlayer.Contracts.IoC.IActivate;
import org.player.JavaPlayer.Contracts.IoC.IDisposable;
import org.player.JavaPlayer.Contracts.IoC.Annotations.Autowired;
import org.player.JavaPlayer.Contracts.Main.IJavaPlayerSupervisor;
import org.player.JavaPlayer.Contracts.Main.IResource;
import org.player.JavaPlayer.Contracts.Views.ITrayView;
import org.player.JavaPlayer.State.PlayerState;

public class TrayView implements ITrayView, IActivate, IDisposable {

    private IResource resources;
    private IJavaPlayerSupervisor supervisor;
	private TrayIcon trayIcon;

    @Override
    @Autowired
    public void setController(IJavaPlayerSupervisor supervisor) {
        this.supervisor = supervisor;
    }

    @Autowired
    public void setResource(IResource resources) {
        this.resources = resources;
    }
    @Override
	public void activate(){
    	init();
    	supervisor.getController().addPlayerStateListener(new PropertyChangeListener(){

			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				int newValue=(int) evt.getNewValue();
				if(newValue==PlayerState.SONG_SET){
					showMessage();
				}
			}});
    }
    protected void showMessage() {
		trayIcon.displayMessage("Now Playing", supervisor.getCurrentSong().getTitle(), TrayIcon.MessageType.INFO);
	}

	private void init() {
        SystemTray tray = SystemTray.getSystemTray();
        trayIcon = new TrayIcon(resources.getIcon());
        trayIcon.addMouseListener(new MouseListener() {
            @Override
            public void mouseEntered(MouseEvent arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void mouseExited(MouseEvent arg0) {
                // TODO Auto-generated method stub
            }

            @Override
            public void mouseReleased(MouseEvent arg0) {
                if (arg0.getButton() == MouseEvent.BUTTON1)
                    ;// TODO show panel with buttons
                else if (arg0.getButton() == MouseEvent.BUTTON2)
                    ;// show popup menu
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                // TODO Auto-generated method stub
            }

            @Override
            public void mousePressed(MouseEvent e) {
                // TODO Auto-generated method stub
            }
        });
        trayIcon.setImageAutoSize(true);
        try {
            tray.add(trayIcon);
        } catch (AWTException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } 
    }

	@Override
	public void deactivate() throws Throwable {
		SystemTray.getSystemTray().remove(trayIcon);
	}
}
